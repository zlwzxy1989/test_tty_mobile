/*
 * 2012.1.5
 * 			#332 新闻列表功能修改
 * 2012.5.7
 * 			#59 新闻列表，读取更多新闻后不出现翻页
 * 			#60 新闻列表，新闻标题中不要出现省略号
 *
 * 2012.5.8. #92 修改到news路径
 * 2012.6.6. ac #111: 基金内页，根据基金所属基金公司读取相关新闻
 * 2012.6.7. ac #115 新闻,新闻列表页改为原生方式生成tableView
 * 2012.6.18 ac #139 基金内页，基金资讯内页部分基金公司返回按钮显示过长
 * 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
//2012.5.8. #92 修改到news路径
win.backgroundImage = GetBackgroundFile(2);

CheckInternet();

//每次读进的新闻数量
var NEWS_NUM = 20;
//是否有更旧的新闻可读
var has_old_news = true;

Ti.API.info('run news list.js');

var actInd = Titanium.UI.createActivityIndicator({
	height : 50,
	width : 10,
	style : Titanium.UI.iPhone.ActivityIndicatorStyle.DARK
});
win.add(actInd);

actInd.show();

var my = {};
my.xhr = Ti.Network.createHTTPClient();
var arr_newsList = [];
var json = [];
//2012.6.6. ac #111: 基金内页，根据基金所属基金公司读取相关新闻
var display_type = Titanium.App.Properties.getString('news_display_type', 'normal');
if(display_type == 'normal') {
	//从新闻列表进入的，按分类读取
	arr_newsList.cat = Titanium.App.Properties.getInt('newsCategory');
} else if(display_type == 'fund_detail') {
	//从基金内页进入的，按tag读取
	arr_newsList.tag = Titanium.App.Properties.getInt('companyTag');
}
arr_newsList.key = 'TTYFUND-CHINAPNR';
arr_newsList.act = 'GetNewsList';
arr_newsList.data_type = 'mobile';
arr_newsList.to = NEWS_NUM;
arr_newsList.url = 'http://www.ttyfund.com/api/services/NewsService.php';
url = getApiUrl(arr_newsList);
Ti.API.info('url:' + url);
my.xhr.open("GET", url);
my.xhr.onload = function() {
	try {
		json = JSON.parse(my.xhr.responseText);
		Ti.API.info('json:' + json);
		if(json.return_code == 1) {
			var news_count = json.news_list.length;
			Ti.API.info('news_count:' + news_count);
			if(news_count < NEWS_NUM) {
				has_old_news = false;
				Ti.API.info('no more news to read in');
			}
			var data = [];
			for( i = 0; i < news_count; i++) {
				// 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
				data.push(json['news_list'][i]['news_title']);
				data.push(json['news_list'][i]['news_date'].substring(0, 10));
				data.push(json['news_list'][i]['news_id']);
			}

			tableviewNewsList = wing.ui.createTableView({
				type : tableviewLabels,
				value : data,
				ui_filename : 'tabNewsList.ui',
				style : Titanium.UI.iPhone.TableViewStyle.PLAIN
			});
		}
		win.add(tableviewNewsList);
		//loading row
		var updating = false;
		var lastDistance = 0;
		var loadingRow = Ti.UI.createTableViewRow();
		loadingRow.title = '新闻数据读入中...';
		loadingRow.color = 'gray';
		loadingRow.height = 43;
		loadingRow.hasChild = false;
		loadingRow.backgroundColor = 'white';
		loadingRow.touchEnabled = false;
		last_news = news_count + 1;
		//新闻列表项目点击事件处理
		tableviewNewsList.addEventListener('click', function(e) {
			var Lables = [];
			Lables = e.row.getChildren();
			Titanium.App.Properties.setInt('newsId', parseInt(Lables[2]['text']));
			//防止用户在两个tab都进入此页导致display_type变量混乱
			Titanium.App.Properties.setString('news_display_type', display_type);
			Ti.API.info("id:" + Lables[2]['text']);
			var win = wing.ui.createWindow({
				url : 'tabNewsDetail.js',
				// 2012.6.18 ac #139 基金内页，基金资讯内页部分基金公司返回按钮显示过长
				title : '     资讯详情     ',
				barColor : 'faa61a'
			});
			win.backgroundImage = GetBackgroundFile();
			Titanium.UI.currentTab.open(win, {
				animated : true
			});
		});
		actInd.hide();
		//翻页事件代码
		function beginUpdate() {
			updating = true;
			win.setRightNavButton(actInd);
			actInd.show();
			tableviewNewsList.appendRow(loadingRow);
			//读取新闻接口，根据返回新闻数判断
			my.xhr_update = Ti.Network.createHTTPClient();
			arr_newsList.from = last_news;
			Ti.API.info('last_news:' + last_news);
			arr_newsList.to = last_news + NEWS_NUM;
			arr_newsList.url = 'http://www.ttyfund.com/api/services/NewsService.php';
			url = getApiUrl(arr_newsList);
			Ti.API.info('update_url:' + url);
			my.xhr_update.open("GET", url);
			my.xhr_update.onload = function() {
				try {
					json = JSON.parse(my.xhr_update.responseText);
					Ti.API.info('json:' + json);
					if(json.return_code == 1) {
						var news_count = json.news_list.length;
						if(news_count < NEWS_NUM) {
							has_old_news = false;
							Ti.API.info('no more news to read in');
						}
						if(news_count == 0)
							return false;
						tableviewNewsList.deleteRow(last_news - 1, {
							animationStyle : Titanium.UI.iPhone.RowAnimationStyle.NONE
						});
						var data = [];
						for( i = 0; i < news_count; i++) {
							data.push(json['news_list'][i]['news_title']);
							data.push(json['news_list'][i]['news_date'].substring(0, 10));
							data.push(json['news_list'][i]['news_id']);
						}
						tableviewNewsList = wing.ui.createTableView({
							type : tableviewLabels,
							value : data,
							hasChild : true,
							ui_filename : 'tabNewsList.ui',
							tableView : tableviewNewsList
						})

					}
					actInd.hide();
					//#59 新闻列表，读取更多新闻后不出现翻页
					//tableviewNewsList.scrollToIndex(last_news + 5);
					last_news = last_news + news_count;
					tableviewNewsList.addEventListener('scroll', scrollEvent);
					updating = false;
					tableviewNewsList.scrollable = true;
				} catch(E) {
					tableviewNewsList.scrollable = true;
					Ti.API.info(E);
				}
			}
			my.xhr_update.send();
		}

		var scrollEvent = function(e) {

			if(has_old_news == false) {
				//ShowInfo('没有更多新闻了');
				tableviewNewsList.removeEventListener('scroll', scrollEvent);
			} else {

				var offset = e.contentOffset.y;
				var height = e.size.height;
				var total = offset + height;
				var theEnd = e.contentSize.height;
				var distance = theEnd - total;

				// going down is the only time we dynamically load,
				// going up we can safely ignore -- note here that
				// the values will be negative so we do the opposite
				if(distance < lastDistance) {
					if(!updating && (total >= theEnd)) {
						tableviewNewsList.removeEventListener('scroll', scrollEvent);
						tableviewNewsList.scrollable = false;
						beginUpdate();
					}
				}
				lastDistance = distance;
			}
		}
		if(last_news != 1)
			tableviewNewsList.addEventListener('scroll', scrollEvent);

	} catch(E) {
		Ti.API.info(E);
	}
}
my.xhr.send();

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile();
});
