/**
 * 基金经理页面 by zxy
 * 2012.6.6. ac #110 基金内页，基金经理页面数据读取、显示
 * 2012.6.13. ac #129 基金内页,基金经理介绍页统一使用全角冒号
 * 2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
 * 2012.6.18 字高由22改为18 和新闻统一
 * 2012.6.20. ac #140 基金内页，二级菜单详情页如“基金详情”详情页，首行添加“基金详情”
 * 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
 */
Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);
//2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
var fund_code = Titanium.App.Properties.getString('fund_code_detail', '');
var fund_name = Titanium.App.Properties.getString('fund_name_detail', '');
if(getStrLength(fund_name) > 20) {
	fund_name = CnSubString(fund_name, 19, true);
}
labelTitle = Ti.UI.createLabel({
	text : fund_name + '\r' + fund_code,
	height : 43,
	color : 'white',
	textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
	width : '70%',
	font : {
		fontSize : 14,
		fontWeight : 'bold'
	},
});
win.setTitleControl(labelTitle);
CheckInternet();
var my = {};
//tableView数据存放数组
my.data = [];
my.getFundManager = Ti.Network.createHTTPClient();
var arr_getFundManager = {};
var json_getFundManager = {};
arr_getFundManager.key = 'TTYFUND-CHINAPNR';
arr_getFundManager.act = 'GetFundManager';
arr_getFundManager.symbol = fund_code;
arr_getFundManager.url = 'http://www.ttyfund.com/api/services/FundService.php';
url_getFundManager = getApiUrl(arr_getFundManager);
Ti.API.info('url_getFundManager:' + url_getFundManager);
my.getFundManager.open("GET", url_getFundManager);
my.getFundManager.onload = function() {
	try {
		Ti.API.info(my.getFundManager.responseText);
		json_getFundManager = JSON.parse(my.getFundManager.responseText);
		//无基金经理信息
		if(json_getFundManager.return_code == 'manager not find') {
			ShowInfo('暂无基金经理信息');
		} else if(json_getFundManager.return_code == 1) {
			managers = json_getFundManager.managers;
			getFundManagerDetail(0, managers);
		}
	} catch(E) {
		Ti.API.info(E);
	}

}
my.getFundManager.send();
/*
 * 获取managers数组中所有基金经理id对应的基金经理详细信息
 * 传入：序号，基金经理数组。
 *  因函数会回调自身，初次调用时序号要写为0
 *  采用回调的方法才能保证请求是依次发生的，直接循环mangers数组会并发请求造成数据处理问题
 * 传出：无
 * */
function getFundManagerDetail(num, managers) {
	manager_id = managers[num]['manager_id'];
	my.getFundManagerDetail = Ti.Network.createHTTPClient();
	var arr_getFundManagerDetail = {};
	var json_getFundManagerDetail = {};
	arr_getFundManagerDetail.key = 'TTYFUND-CHINAPNR';
	arr_getFundManagerDetail.act = 'GetFundManagerDetail';
	arr_getFundManagerDetail.symbol = fund_code;
	arr_getFundManagerDetail.manager_id = manager_id;
	arr_getFundManagerDetail.url = 'http://www.ttyfund.com/api/services/FundService.php';
	url_getFundManagerDetail = getApiUrl(arr_getFundManagerDetail);
	Ti.API.info('url_getFundManagerDetail:' + url_getFundManagerDetail);
	my.getFundManagerDetail.open("GET", url_getFundManagerDetail);
	my.getFundManagerDetail.onload = function() {
		try {
			Ti.API.info(my.getFundManagerDetail.responseText);
			json_getFundManagerDetail = JSON.parse(my.getFundManagerDetail.responseText);
			if(json_getFundManagerDetail.return_code == 1) {
				//获取相关信息
				// 2012.6.13. ac #129 基金内页,基金经理介绍页统一使用全角冒号
				// 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
				my.data.push(json_getFundManagerDetail.manager + '：' + json_getFundManagerDetail.degree);
				my.data.push('任职日期：' + json_getFundManagerDetail.begin_date);
				my.data.push(Trim(json_getFundManagerDetail.career));
			}
			if(manager_id == managers[managers.length-1]['manager_id']) {
				//如果该manager_id是传入数组的最后一个，则开始生成tableView
				showTableView();
			} else {
				//还未到最后一个，则继续用下一个manager_id获取数据
				getFundManagerDetail(num + 1, managers);
			}
		} catch(E) {
			Ti.API.info(E);
		}

	}
	my.getFundManagerDetail.send();
}

/*
 * 显示tableView
 * 传入：无
 * 传出：无
 * */
function showTableView() {

		tableview = wing.ui.createTableView({
			type : tableviewLabels,
			value : my.data,
			hasChild : false,
			ui_filename : 'tabFundDetailManager.ui',
			header:{"index":[0],"text":["基金经理信息"]}
		})
	win.add(tableview);

}

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
