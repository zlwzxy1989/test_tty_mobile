/**
 * 基金基本信息 by zxy
 * 2012.5.31. ac #104 基金内页，基金基本信息页面数据读取
 * 2012.5.31. ac #105 基金内页，基金基本信息页面数据显示
 * 2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
 * 2012.6.18. ac #140 基金内页，二级菜单详情页如“基金详情”详情页，首行添加“基金详情”
 * 2012.6.18. ac #142 基金内页，有二级目录的标题与新闻列表格式统一
 * 2012.6.24. #166: 基金内页，基金基本信息内页，“基金基本信息”字体不统一
 * 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);
//2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
var fund_code = Titanium.App.Properties.getString('fund_code_detail', '');
var fund_name = Titanium.App.Properties.getString('fund_name_detail', '');
if(getStrLength(fund_name) > 20) {
	fund_name = CnSubString(fund_name, 19, true);
}
labelTitle = Ti.UI.createLabel({
	text : fund_name + '\r' + fund_code,
	height : 43,
	color : 'white',
	textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
	width : '70%',
	font : {
		fontSize : 14,
		fontWeight : 'bold'
	},
});
win.setTitleControl(labelTitle);
CheckInternet();
var isCurrency = Titanium.App.Properties.getBool('isCurrency', false);
var my = {};
my.getFundDetail = Ti.Network.createHTTPClient();
var arr_getFundDetail = {};
var json_getFundDetail = {};
arr_getFundDetail.key = 'TTYFUND-CHINAPNR';
arr_getFundDetail.act = 'GetFundInfo';
arr_getFundDetail.symbol = fund_code;
arr_getFundDetail.url = 'http://www.ttyfund.com/api/services/FundService.php';
url_getFundDetail = getApiUrl(arr_getFundDetail);
Ti.API.info('url_getFundDetail:' + url_getFundDetail);
my.getFundDetail.open("GET", url_getFundDetail);
my.getFundDetail.onload = function() {
	try {
		Ti.API.info(my.getFundDetail.responseText);
		json_getFundDetail = JSON.parse(my.getFundDetail.responseText);
		if(json_getFundDetail.return_code == 1) {
			fund_found_date = dateFormat(json_getFundDetail.inception_date);
			fund_type = json_getFundDetail.category_local;
			fund_rating = json_getFundDetail.rating_3_year || '';
			fund_total_share = json_getFundDetail.issue_volume;
			fund_buy_state = json_getFundDetail.stop_type;
			fund_sell_state = json_getFundDetail.restore_type || '';
			if(isCurrency) {
				showTableView();
			} else {
				getFundFee();
			}
		}
	} catch(E) {
		Ti.API.info(E);
	}

}
my.getFundDetail.send();
function getFundFee() {
	my.getFundFee = Ti.Network.createHTTPClient();
	var arr_getFundFee = {};
	var json_getFundFee = {};
	arr_getFundFee.key = 'TTYFUND-CHINAPNR';
	arr_getFundFee.act = 'GetFundFee';
	arr_getFundFee.symbol = fund_code;
	arr_getFundFee.url = 'http://www.ttyfund.com/api/services/FundService.php';
	url_getFundFee = getApiUrl(arr_getFundFee);
	Ti.API.info('url_getFundFee:' + url_getFundFee);
	my.getFundFee.open("GET", url_getFundFee);
	my.getFundFee.onload = function() {
		try {
			Ti.API.info(my.getFundFee.responseText);
			json_getFundFee = JSON.parse(my.getFundFee.responseText);
			if(json_getFundFee.return_code == 1) {
				fund_bank_fee = json_getFundFee['in'][0].bank_fee;
				fund_tty_fee = json_getFundFee['in'][0].tty_fee;
				showTableView();
			}
		} catch(E) {
			Ti.API.info(E);
		}

	}
	my.getFundFee.send();
}

function showTableView() {
	// 2012.6.18. ac #142 基金内页，有二级目录的标题与新闻列表格式统一
	// 2012.6.24. #166: 基金内页，基金基本信息内页，“基金基本信息”字体不统一
	// 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
	data = [];
	data.push('成立日期：' + fund_found_date);
	data.push('');	
	data.push('申购：' + fund_buy_state);
	data.push('赎回：' + fund_sell_state);
	data.push('基金类型：' + fund_type);
	data.push('海通评级：' + fund_rating);
	if(!isCurrency) {
	data.push('银行柜台费率：' + parseFloat(fund_bank_fee).toFixed(2) + '%');
	data.push('天天盈费率：' + parseFloat(fund_tty_fee).toFixed(2) + '%');	
	} else {
	data.push('费率：0%');
	data.push('');
	}
	data.push('发行总份额：' + fund_total_share + ' 亿份');
	data.push('');
	var header={"index":[0],"text":["基金基本信息"]};
	tableviewFund = wing.ui.createTableView({
		backgroundColor : 'transparent',
		type : tableviewLabels,
		value : data,
		ui_filename : 'tabFundDetailInfo.ui',
		header:header,
	});
	win.add(tableviewFund);
	function getFundTableviewRow(_args) {
		row = Ti.UI.createTableViewRow({
			height : 43,
			width : '100%',
			font : _args.font || {
				fontSize : 14,
				fontWeight : 'bold'
			},
			backgroundColor : 'white'
		});
		if(_args.header != null) {
			row.header = _args.header;
		}
		for(var j = 0; j <= 1; j++) {
			//不需要根据数字显示不同的颜色
			label = Ti.UI.createLabel({
				text : _args[j].text,
				height : 42,
				font : {
					fontWeight : 'bold',
					fontSize : 14,
					fontFamily : 'Arial'
				}
			});
			//根据显示数据类型不同调整label位置
			if(j == 0) {
				label.left = 10;
				label.width = 200;
			} else {
				label.left = 155;
				label.width = 144;
			}
			label.zIndex = j;
			row.add(label);
		}

		return row;
	}

}

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
