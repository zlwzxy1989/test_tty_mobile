/*
 *  tty_tools.js 所有天天盈理财手机客户端需要调用的函数等，从tools.js移植部分
 *  2012.5.19. #94 移动到 /tty_tools/路径
 */

//PostBE, 将用户使用情况传送到服务器
//2011.10.4. #164 系统，每次启动app时候增加用户行为访问记录
//2011.10.18. #241 行为记录函数PostBE加入appID，以适用多个应用
//2011.11.8. #304 调用检查internet是否链接修改参数为不显示警告信息
//2012.3.4. #522 postbe，修改本地调用方式,增加是否发送过判断
//2012.3.10. #542  2012-3-9 样式日期格式有错误
//2012.5.14 #84 POSTBE接口修改
function PostBE(appID) {
	UID = Titanium.App.Properties.getString('UID', '');
	if(UID == '') {
		Ti.API.info('UID empty,begin to get UID');

		var xhr_getUID = Ti.Network.createHTTPClient();
		var arr_getUID = {};
		var json = [];
		var lID = Titanium.Platform.macaddress;
		arr_getUID.key = 'TTYFUND-CHINAPNR';
		arr_getUID.act = 'GetUID';
		arr_getUID.url = 'http://www.ttyfund.com/api/services/MobileService.php';
		arr_getUID.mac_id = lID;
		url = getApiUrl(arr_getUID);
		Ti.API.info('url:' + url);
		xhr_getUID.open("GET", url);
		xhr_getUID.onload = function() {
			try {
				Ti.API.info('return text:' + xhr_getUID.responseText);
				json = JSON.parse(xhr_getUID.responseText);
				Ti.API.info('json:' + json);
				if(json.return_code == 1) {
					UID = json.uid;
					Titanium.App.Properties.setString('UID', UID);
					doPostBE(UID);
				}
			} catch(E) {
				Ti.API.info(E);
			}
		}
		xhr_getUID.send();
	} else {
		Ti.API.info('UID found:' + UID);
		doPostBE(UID);
	}
	function doPostBE(UID) {
		//check local date mark
		var ldPostBE = Titanium.App.Properties.getString('PostBE_Date');

		if(ldPostBE != null) {
			if(ldPostBE.length < 10) {
				ldPostBE = ''
			}
		};
		//get channel
		var channel = '';
		f = Titanium.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'channel/channel.txt');
		contents = f.read.blob;
		Ti.API.info('channel file = ' + f);
		Ti.API.info('channel file ' + f.exists());
		var lini = CSVToArray(contents.text,'=');
		if(lini[0][1] != null) {
			channel = lini[0][1];
			Ti.API.info('channel = ' + channel);
		}
		var ldToday = getDate();
		Ti.API.info('postbe date:' + ldPostBE);
		Ti.API.info('today:' + ldToday);

		if((ldPostBE == null) || (ldPostBE < ldToday)) {

			Ti.API.info('do postbe');

			// INFO: map a client to a call:
			var xhr = Titanium.Network.createHTTPClient();

			// INFO: handle successful query:
			xhr.onload = function() {
				Ti.API.info('Post BE ok.');
				Titanium.App.Properties.setString('PostBE_Date', ldToday);
			}
			// INFO: handle errors:
			xhr.onerror = function() {
				Ti.API.info('Post BE error.');
			}
			// INFO: define URL for query:
			arr_PostBE = {};
			arr_PostBE.key = 'TTYFUND-CHINAPNR';
			arr_PostBE.act = 'PostBE';
			arr_PostBE.url = 'http://www.ttyfund.com/api/services/MobileService.php';
			arr_PostBE.date = getDateTime();
			arr_PostBE.ip = Titanium.Platform.address;
			arr_PostBE.ver = appID;
			arr_PostBE.id = Titanium.Platform.macaddress;
			arr_PostBE.UUID = Titanium.Platform.UUID;
			Ti.API.info('uuid:'+arr_PostBE.UUID);
			arr_PostBE.UID = UID;
			arr_PostBE.model = Titanium.Platform.model;
			arr_PostBE.channel = channel;
			url = getApiUrl(arr_PostBE);
			Ti.API.info('url:' + url);
			xhr.open("GET", url);
			if(CheckInternet(false)) {
				xhr.settimeout = 5000;
				xhr.open('GET', url);
				Ti.API.info('date:' + getDateTime() + '|ip: ' + Titanium.Platform.address + '|ver: ' + appID + '|id: ' + Titanium.Platform.macaddress + '|UID: ' + UID + '|' + '|model: ' + Titanium.Platform.model + '|channel: ' + channel);
				xhr.send();
			}
		}
	}

}