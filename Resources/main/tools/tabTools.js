/*

 2011.12.20. #409 界面，理财工具界面调整
 2011.12.17. #397 外汇兑换图标设置
 -----
 2011.6.25.
 2011.10.5. #186 增加论坛链接
 增加data[]数据
 增加tableviewTools.addEventListener('click',
 2011.10.6. #194 修改win bar背景色
 2011.10.8. #224 调用browser显示内容的地方增加设置bar背景颜色
 tableviewTools.addEventListener('click'...
 2011.10.8. #226 工具，窗口设置背景色等
 2011.11.1. #281 设置按钮从主窗口移动到理财工具
 2011.11.26. #354 调整调用库函数方式
 2012.1.4. tableview修改为配置文件模式
 2012.2.20. #511 理财工具界面，“天天盈论坛”显示与整体分类小标题显示不一致
 2012.2.24. #516 调整显示web调用方式
 2012.3.11. #543 调整工具和设置菜单
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;
//橙色 //#194 修改win bar背景色
win.barColor = 'faa61a';
//2011.10.8. #226 工具，窗口设置背景色等
win.backgroundImage = GetBackgroundFile(2);

//2011.11.1. #281 设置按钮从主窗口移动到理财工具
var btnSetup = Titanium.UI.createButton({
	image : '../../images/system/btn_setup.png'
});

btnSetup.addEventListener('click', function(e) {

	var w = Titanium.UI.createWindow({
		url : 'tabSetup.js',
		title : '更多功能'
	})

	Titanium.UI.currentTab.open(w, {
		animated : true
	});
});
win.rightNavButton = btnSetup;
//#281 end


//2012.1.4. #436 修改为配置文件模式
var tableviewTools = wing.ui.createTableView({
	type : tableviewSimple,
	ui_filename : 'tabTools.ui',
	dir_level:2
})

//2011.10.8. #226 工具，窗口设置背景色等
//2011.10.8. #224 所有调用browser显示内容的地方增加设置bar背景颜色
//2011.10.5. #186 增加论坛链接
// create table view event listener
//2012.2.20. #511 理财工具界面，“天天盈论坛”显示与整体分类小标题显示不一致
//2012.2.24. #516 调整显示web调用方式
//2012.3.11. #543 调整工具和设置菜单
tableviewTools.addEventListener('click', function(e) {

	switch (e.index) {
		case 0:
			var win = Titanium.UI.createWindow({
				url : 'tabChinaIndex.js',
				title : e.rowData.title
			})

			Titanium.UI.currentTab.open(win, {
				animated : true
			})

			break;

		case 1:
			var web = wing.ui.createWeb({
				Url : 'http://www.ttyfund.com/api/mobile/ttyfundten/index.html',
				Title : '基金投资风格测测看',
				CloseNav : true,
				BarColor : 'faa61a'
			});

			break;
		case 2:
			var winFund = Titanium.UI.createWindow({
				url : 'tabExchange.js',
				title : e.rowData.title
			})

			Titanium.UI.currentTab.open(winFund, {
				animated : true
			})

			break;
		case 3:
			var web = wing.ui.createWeb({
				Url : 'http://bbs.ttyfund.com/forum.php?gid=1',
				Title : '天天盈帮助中心',
				CloseNav : false,
				BarColor : 'faa61a',
				dir_level:2
			});
			break;
		case 4:
			var web = wing.ui.createWeb({
				Url : 'http://bbs.ttyfund.com/forum-54-1.html',
				Title : 'iPhone版讨论',
				CloseNav : false,
				BarColor : 'faa61a',
				dir_level:2
			});
			break;
		case 5:
			var web = wing.ui.createWeb({
				Url : 'http://www.ttyfund.com/client/iphone/',
				Title : '天天盈-手机端',
				CloseNav : false,
				BarColor : 'faa61a',
				dir_level:2
			});
			break;
		case 6:
			var web = wing.ui.createWeb({
				Url : 'http://www.ttyfund.com',
				Title : '天天盈',
				CloseNav : false,
				BarColor : 'faa61a',
				dir_level:2
			});
			break;
	};
});
win.add(tableviewTools);

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
