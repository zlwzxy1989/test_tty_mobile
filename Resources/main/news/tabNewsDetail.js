/**
 * @author zxy
 * news detail js created on 2012.4.25
 * 2012.5.6
 * 			#49 新闻详细页，UI设计
 * 			#51 新闻详细页，新闻详细内容读入和处理
 * 			#52 新闻详细页，添加返回顶部按钮响应事件
 * 2012.5.7
 * 			#64 新闻详细页，部分文章最后一段显示不全
 * 			#65 新闻详细页，文章底部留白长度不统一
 * 			#56 新闻详细页，标题字号变大，时间字号变小颜色为灰色
 * 			#57 新闻详细页，底部留白去除
 * 			#58 新闻详细页，回到顶部按钮与天天盈登录中“关闭”按钮一致
 * 2012.5.8
 * 			#67 基金新闻，新闻显示解析引擎错误
 * 2012.5.10. 修改文本框高度计算
 *
 * 2012.5.8. #92 修改到news路径
 * 2012.6.6. ac #102 基金内页，首页读取基金当天净值信息、基金所属基金公司的tag的ID
 * 2012.6.6. ac #103 基金内页，首页根据基金种类显示不同的内容
 * 2012.6.6. ac #111: 基金内页，根据基金所属基金公司读取相关新闻
 * 2012.6.16. var WORD_HEIGHT = 18; 可能sdk 2.0.2 相关
 * 2012.8.2 去除几行调整height的无效代码 by zxy
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

//2012.5.8. #92 修改到news路径
var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);
CheckInternet();

var actInd = Titanium.UI.createActivityIndicator({
	height : 50,
	width : 10,
	style : Titanium.UI.iPhone.ActivityIndicatorStyle.DARK
});
win.add(actInd);

actInd.show();

var my = {};
my.xhr = Ti.Network.createHTTPClient();

news_id = Ti.App.Properties.getInt('newsId', 0);
//alert(news_id);
var array = [];
//var json = {};
array.key = 'TTYFUND-CHINAPNR';
array.act = 'GetNewsDetail';
array.news_id = news_id;
array.url = 'http://www.ttyfund.com/api/services/NewsService.php';
urlNewsDetail = getApiUrl(array);
//alert(urlNewsDetail);
my.xhr.open("GET", urlNewsDetail);
my.xhr.onload = function() {
	try {
		my.data = [];
		json = JSON.parse(my.xhr.responseText);
		if (json.return_code == 1) {

			title = json['news_title'];
			pubDate = json['news_date'].substring(0, 10);
			content = json['news_content'];
			//处理文章内容
			content = content.replace(/<tr[^>]*>[^\0]*?<\/tr>/img, function(str) {
				return str.replace(/<\/td>/, ':')
			});
			content = content.replace(/<tr[^>]*>[^\0]*?<\/tr>/img, function(str) {
				return str.replace(/<[\/]?(a|p|span|img|colgroup|col|strong|font|table|tbody|thead|div)[^>]*>/img, '')
			});
			content = content.replace(/<tr[^>]*>[^\0]*?<\/tr>/img, function(str) {
				return str.replace(/\s/img, '')
			});
			content = content.replace(/<tr[^>]*>[^\0]*?<\/tr>/img, function(str) {
				return str.replace(/<\/td>/img, ' ')
			});
			content = content.replace(/<[\/]?(a|p|span|tr|td|img|colgroup|col|strong|font|table|tbody|thead|div)[^>]*>/img, '');
			content = content.replace(/<!--.*?-->/img, '');

			content = content.replace(/(查看基金详细&gt;&gt;|&nbsp;|还没有开通天天盈账户？|我要认购|我要购买)/img, '');
			content = content.replace(/(\r\n)+/img, '\n\n');
			content = content.replace(/(\s){2,}/img, '\n\n');
			content = Trim(content);
			content = content + '\n\n';
			Ti.API.info('content:' + content);
			//view for news title & content
			var scrollView = Ti.UI.createScrollView({
				contentHeight : 'auto',
				contentWidth : 'auto',
				height : '100%',
				width : 320,
				top : 0,
				showVerticalScrollIndicator : true,

			});
			var viewTitle = Ti.UI.createView({
				height : 50,
				width : 310,
				top : 5,
				borderWidth : 1,
				borderColor : '#bbb',
				borderRadius : 5,
				backgroundColor : 'white',
			})
			//#56 新闻详细页，标题字号变大，时间字号变小颜色为灰色
			var labelTitle = Ti.UI.createLabel({
				text : title,
				color : 'black',
				top : 7,
				left : 10,
				width : 300,
				height : Ti.UI.SIZE,
				font : {
					fontSize : 16,
					fontWeight : 'bold',
				},
				wordWrap : false
			});
			var labelDate = Ti.UI.createLabel({
				text : '日期:' + pubDate,
				color : 'darkgray',
				bottom : 2,
				width : 120,
				left : 110,
				height : 20,
				font : {
					fontSize : 12,

				}
			});
			var labelCat = Ti.UI.createLabel({
				text : '分类:' + Titanium.App.Properties.getString('newsCatCN', ''),
				color : 'darkgray',
				bottom : 2,
				width : 100,
				right : 0,
				height : 20,
				font : {
					fontSize : 12,

				}
			});
			//2012.6.6.ac #111: 基金内页，根据基金所属基金公司读取相关新闻
			var display_type = Titanium.App.Properties.getString('news_display_type', 'normal');
			if (display_type == 'fund_detail') {
				//从基金内页进入到此，则不显示新闻分类，同时调整日期label便于美观
				labelCat.text = '';
				labelDate.left = 170;
			}
			viewTitle.add(labelTitle);
			viewTitle.add(labelDate);
			viewTitle.add(labelCat);
			//每行字数
			var WORD_COUNT = 42;
			//字高
			//2012.6.16. 调整，可能是sdk 2.0.2 相关
			var WORD_HEIGHT = 18;
			var line = 0;
			content_tmp = content.match(/.*?\n\n/img);
			if (content_tmp != null) {

				for ( i = 0; i < content_tmp.length; i++) {
					line_add = Math.ceil(getStrLength(Trim(content_tmp[i])) / WORD_COUNT);
					line = line + line_add;

					Ti.API.info('length:' + getStrLength(Trim(content_tmp[i])) + '|para: ' + i + ':' + line_add + ' line(s)');
				}
				line = (line + 2 + content_tmp.length);
			} else
				line = line + 2 + Math.ceil(getStrLength(Trim(content)) / WORD_COUNT);
			Ti.API.info('line total:' + line);
			contentHeight = line * WORD_HEIGHT;
			var textAreaContent = Ti.UI.createTextArea({
				value : content,
				backgroundColor : 'white',
				textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
				width : 310,
				top : 65,
				font : {
					fontSize : 14
				},
				borderWidth : 1,
				borderColor : '#bbb',
				borderRadius : 5,
				height : contentHeight,
				editable : false,
				scrollable : false,
			});

			//scrollView.add(textAreaTitle);
			scrollView.add(viewTitle);
			//scrollView.add(textAreaContent);
			scrollView.add(textAreaContent);
			//return to top button
			var btnReturnTop = Titanium.UI.createButton({
				title : '回到顶部',
			});
			//#52 新闻详细页，添加返回顶部按钮响应事件
			btnReturnTop.addEventListener('click', function(e) {
				scrollView.scrollTo(0, 0);

			})
			win.add(scrollView);
			win.rightNavButton = btnReturnTop;
		}

		actInd.hide();
	} catch(E) {
		alert(E);
	}
};
my.xhr.send();
