/*
 * js for email feedback
 * 2012.2.26 created
 * 
 * 2012.3.8. paste from zhen.x.y.
 * 2012.3.9 #537 用户反馈，文本输入这里文字太小，宁愿提示文字小一些
 * 2012.3.9 #538 用户反馈，成功提交后，按钮显示的是ok，不是中文
 * 2012.3.9 #539 用户反馈，成功后还是停留在反馈页面，应该自动回到前面
 * 2012.3.13 #547 用户反馈，默认的反馈内容，不应该支持提交
 * 
 2012.4.28. ac #43 用户反馈功能label高度警告
 */
Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');
Ti.include('../../tty_tools/consts.js');
//
var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);
my = {};

//alert dialog
var arrAlert = [];
arrAlert.title = win.title;

var scrollView = Ti.UI.createScrollView({
	contentHeight : 'auto',
	contentWidth : 'auto'
});

//btn for post data
btn = Ti.UI.createButton({
	title : '提交'
});
btn.addEventListener('click', function(e) {

	var comment = new String(areaComment.value);
	var mail = new String(textFieldMail.value);
	//check textarea
	if(checkStr('invalidSymbol', comment) == true) {
		arrAlert.message = '输入内容存在非法字符，请检查。';
		showAlert(arrAlert);
		areaComment.focus();
		return false;
	} else if(getStrLength(comment) == 0) {
		arrAlert.message = '输入内容不能为空。';
		showAlert(arrAlert);
		areaComment.focus();
		return false;
	// 2012.3.13 #547 用户反馈，默认的反馈内容，不应该支持提交
	} else if(areaComment.value == defaultComment) {
		arrAlert.message = '请输入意见或建议。';
		showAlert(arrAlert);
		areaComment.focus();
		return false;

	}

	//check email
	else if(checkStr('email', mail) == false && mail.length != 0) {
		arrAlert.message = '邮件格式不正确，请重新填写。';
		showAlert(arrAlert);
		textFieldMail.focus();
		return false;
	} else {
		//waiting for post
		var actInd = Titanium.UI.createActivityIndicator({
			height : 50,
			width : 10,
			style : Titanium.UI.iPhone.ActivityIndicatorStyle.DARK
		});
		win.add(actInd);
		actInd.show();
		//disable window touch
		setDisable();	
		var remoteUrl = 'http://www.ttyfund.com/api/mobile/email.php';
		var id = Titanium.Platform.macaddress;
		var postLevel = labelLevelCH.text;
		try {
			my.xhr = Ti.Network.createHTTPClient();
			my.xhr.open('POST', remoteUrl);
			my.xhr.onload = function() {
				actInd.hide();
				Ti.API.info('app:' + app_name + app_version + '|id:' + id + '|comment:' + comment + '|mail:' + mail + '|level:' + postLevel);
				arrAlert.message = '您的意见已经成功提交，感谢您对我们产品的支持。';
				showAlert(arrAlert);
				//#539 用户反馈，成功后还是停留在反馈页面，应该自动回到前面
				win.close();
			}
			my.xhr.send({
				app : app_name + app_version,
				id : id,
				comment : Trim(areaComment.value),
				mail : textFieldMail.value,
				level : postLevel
			});
			my.xhr.timeout = 8000;
			my.xhr.onerror = function() {
				arrAlert.message = '服务器连接超时，请稍后再试。';
				showAlert(arrAlert);
				actInd.hide();
				setAble();
			};
			my.xhr.onsendstream = function(e) {
				Ti.API.info('sending message,progress:' + e.progress);
			}
		} catch(e) {
			Ti.API.info(e);
		}
	}
});
if(CheckInternet(true) == false) {
	btn.touchEnabled = false;
}
win.rightNavButton = btn;

//label for info
/*labelInfo = Ti.UI.createLabel({
	top : 20,
	left : 40,
	width : 250,
	height : 'auto',
	text : '感谢您使用“天天盈基金理财”，如果您在使用过程中有任何意见或建议，欢迎反馈给我们！',
	font : 13,
});
scrollView.add(labelInfo);
*/
labelComment = Ti.UI.createLabel({
	top : 10,
	left : 10,
	width : 'auto',
	height : 20,
	text : '反馈内容：',
});
scrollView.add(labelComment);

//comment area
var defaultComment='感谢您使用“天天盈基金理财”，请填写您的反馈信息。'; 
areaComment = Ti.UI.createTextArea({
	top : 40,
	width : 260,
	height : 170,
	textAlign : 'left',
	borderRadius : 10,
	value : defaultComment,
	borderColor : 'gray',
	borderWidth : 2,
	//#537 用户反馈，文本输入这里文字太小，宁愿提示文字小一些
	font:{fontSize:16}
	//color : '#888',

});
areaComment.addEventListener('focus', function(e) {
	if(areaComment.value == defaultComment) {
		areaComment.value = '';
		areaComment.color = '';
	}
});
areaComment.addEventListener('blur', function(e) {
	if(areaComment.value == '') {
		//areaComment.color = '#888';
		areaComment.value = defaultComment;

	}
});
//check if content is longer than 100words
areaComment.addEventListener('change', function(e) {
	var comment = new String(areaComment.value);
	if(getStrLength(comment) > 200) {
		Ti.API.info('comment length:' + getStrLength(comment));
		areaComment.value = comment.substring(0, comment.length - 1);
		areaComment.fireEvent('change');
	}
});
scrollView.add(areaComment);
labelLevel = Ti.UI.createLabel({
	top : 220,
	left : 10,
	width : 'auto',
	height : 20,
	text : '请您评价：',
});
scrollView.add(labelLevel);
labelLevelCH = Ti.UI.createLabel({
	top : 220,
	right : 10,
	width : 'auto',
	height : 20,
	text : '一般',
});
scrollView.add(labelLevelCH);
//star

var imageGray = '../../images/system/btn_remove_fund.png';
var imageBlue = '../../images/system/btn_add_fund.png';
var images = [imageBlue, imageBlue, imageBlue, imageGray, imageGray];
var imageNum = images.length;
var level = ['差', '较差', '一般', '较好', '很好'];
var imageViews = [];
var i;
for( i = 0; i < imageNum; i++) {
	imageView = Ti.UI.createImageView({
		width : 'auto',
		height : 'auto',
		top : 250,
		left : 70 + i * 40,

		image : images[i],
	});
	imageViews.push(imageView);
}

function changeStarListener(imageView, i) {
	imageView.addEventListener('click', function(e) {

		for( j = 0; j <= i; j++) {
			imageViews[j].image = imageBlue;
		}
		for( j = i + 1; j < imageNum; j++) {
			imageViews[j].image = imageGray;
		}
		labelLevelCH.text = level[i];
	});
}

for( i = 0; i < imageNum; i++) {
	changeStarListener(imageViews[i], i);
	scrollView.add(imageViews[i]);
}
labelMail = Ti.UI.createLabel({
	top : 290,
	left : 10,
	width : 'auto',
	height : 20,
	text : '您的邮箱：',
});
scrollView.add(labelMail);

var textFieldMail = Titanium.UI.createTextField({
	height : 35,
	top : 320,
	width : 260,
	borderStyle : Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	hintText : '选填，以便我们给您回复',
	keyboardType : Titanium.UI.KEYBOARD_EMAIL,
});
scrollView.add(textFieldMail);
win.add(scrollView);
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile();
});
function getStrLength(str) {
	str = Trim(str);
	var chineseRegex = /[^\x00-\xff]/g;
	var strLength = str.replace(chineseRegex, "**").length;
	return strLength;
}

// may add the function to tools.js
function checkStr(type, s) {
	var patt1;
	switch (type) {
		case 'email':
			patt1 = new RegExp("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$");
			break;
		case 'invalidSymbol':
			patt1 = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/~￥]");
			break;
	};
	return patt1.test(s);
}

function setDisable() {
	win.touchEnabled = false;
	btn.enabled = false;
}

function setAble() {
	win.touchEnabled = true;
	btn.enabled = true;
}

function showAlert(_arr) {
	var dialog = Titanium.UI.createAlertDialog({
		title : _arr.title || '提示',
		//#538 用户反馈，成功提交后，按钮显示的是ok，不是中文
		buttonNames:_arr.names || ['确定'],
	});
	dialog.message = _arr.message;
	dialog.show();
}