/*

 FileName: tabFundList.js
 Function:显示指定类别基金列表
 2011.6.2. created
 2011.6.10. edit to simple interface
 2011.6.11. add tableview footer
 edit multi-fund type
 2011.6.12. edit for loading tableview data delay
 2011.6.15. 优化速度和细节
 2011.6.17. 界面优化
 2011.6.20. shake调整
 2011.6.21. 细节优化
 2011.7.17. 开始使用myApp,可视控件对象化
 2011.9.12. edit for remove android switch
 2011.9.28. #175 基金列表，修改为新的UI呈现方式
 function TableViewAppendRow(beginRow,endRow)
 2011.10.6. #194 修改win bar背景色
 2011.10.8. #197 基金列表页，基金数据日期没有显示
 function TableViewAppendRow(beginRow,endRow)
 2011.10.9. #230 app crash, 可能是内存泄露
 win.addEventListener('blur', function(e) {
 2011.10.14 #236 基金列表页，去除进入时候的日期显示
 2011.10.17. #239 三个月和六个月标题显示回报率
 function TableViewAppendRow(beginRow,endRow)
 #249 去除搜索框
 2011.11.26. #354 调整调用库函数方式
 2012.3.23. #568 基金列表，向下滑动显示更多时候定位修改
 2012.6.20. #147 关注基金、基金列表和基金内页，对于日涨幅数据的描述不统一
 2012.6.22. 根据fundListDisplayType决定显示基金的样式
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

//申明变量
var win = Titanium.UI.currentWindow;
var my = {};

//获得用户在前一个窗口点击的基金列表顺序
var FundKind = Titanium.App.Properties.getString('UserFundKind');
Titanium.API.info('i am fundlist window, user chose : ' + FundKind);

var LocalFundFileDate = Titanium.App.Properties.getString('LocalFundFileDate');

// tableview object
var tableviewFundList = Titanium.UI.createTableView({
	backgroundColor : 'white'
});

//
var lcsv = [];
var lCount, FundCount;

var FundData = [];

// 文件相关变量
var checkfile;
var f;
var contents;
var lcsv0;

//显示tableview相关变量
var FundCode, FundNet, FundReturn, FundName;

//显示更多相关变量
var lastRow;
var navActInd = Titanium.UI.createActivityIndicator();

var updating = false;

var loadingRow = Ti.UI.createTableViewRow();

var lastDistance = 0;

//#194 修改win bar背景色
win.barColor = 'faa61a';
//橙色
win.backgroundColor = '#fff';

//tableview显示行数
my.DisplayRows = 30;

Ti.Gesture.addEventListener('shake', function(e) {
	ShowInfo('基金数据更新于' + LocalFundFileDate);
});
//2011-6-11 6-29
//读入本地funds xml file，基金代码列表
//根据用户选择读取不同基金列表

//读出基金数据数组
FundData = Titanium.App.Properties.getList('FundData');
Ti.API.info('FundData array:' + FundData[1].FundNet);

//获得基金数组长度
lCount = FundData.length;
fundListDisplayType=Titanium.App.Properties.getString('fundListDisplayType');
Ti.API.info('fundListDisplayType: '+fundListDisplayType);
//2012.6.22. 根据fundListDisplayType决定显示基金的样式
if (fundListDisplayType=='fund')
{
//从tabfund.js进入的情况
//如果选择所有基金，直接复制数组
if(FundKind == 8) {
	lcsv = FundData;
};

if(FundKind < 8) {
	//从FundKind 0-7，循环获得本基金的临时数组
	for( i = 0; i < lCount; i++) {
		//检查数组FundKind字段是否等于用户选择基金类别
		if(FundData[i].FundKind == FundKind) {
			lcsv.push({
				FundCode : FundData[i].FundCode,
				FundName : FundData[i].FundName,
				FundNet : FundData[i].FundNet,
				FundReturn : FundData[i].FundReturn
			});
		};
	};
};

//排行榜
if(FundKind > 8) {
	Ti.API.info('Fund Topten');

	//读入本地funds xml file，基金代码列表
	checkfile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'fundlist' + (FundKind - 3) + '.csv');
	f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'fundlist' + (FundKind - 3) + '.csv');
	if (f.exists()==true)
	{
	Ti.API.info('file = ' + f);
	contents = f.read.blob;

	lcsv0 = CSVToArray(contents.text, ';');
	FundCount = lcsv0.length;

	for( i = 0; i < FundCount - 1; i++) {
		lcsv.push({
			FundCode : lcsv0[i][0],
			FundName : lcsv0[i][1],
			FundNet : lcsv0[i][2],
			FundReturn : lcsv0[i][3]
		});
	}
}
	//Ti.API.info(lcsv);
};
}
else if (fundListDisplayType=='company')
{
	//从基金公司进入
	var companyId=Ti.App.Properties.getString('companyId');
	Ti.API.info('company id: ' + companyId);	
	for( i = 0; i < lCount; i++) {
		//检查数组FundKind字段是否等于用户选择基金类别

		if(FundData[i].FundCompanyId == companyId) {
		Ti.API.info(FundData[i].FundCompanyId);			
			lcsv.push({
				FundCode : FundData[i].FundCode,
				FundName : FundData[i].FundName,
				FundNet : FundData[i].FundNet,
				FundReturn : FundData[i].FundReturn
			});
		}
	}
}
FundCount = lcsv.length;
Ti.API.info('FundCount:' + FundCount);

//2011.10.17. #239 三个月和六个月标题显示回报率
//2011.10.8. #197 基金列表页，基金数据日期没有显示
//2011.9.28.#175 基金列表，修改为新的UI呈现方式
//2011.6.11. 6.29. edit
function TableViewAppendRow(beginRow, endRow) {

	Ti.API.info('begin row:' + beginRow + ' end row:' + endRow);

	//读取当前基金文件日期
	var LocalFundFileDate = Titanium.App.Properties.getString('LocalFundFileDate');
	Ti.API.info('fund date:' + LocalFundFileDate);

	for( i = beginRow; i < endRow + 1; i++) {
		FundCode = lcsv[i].FundCode;
		FundName = lcsv[i].FundName;
		FundNet = lcsv[i].FundNet;
		FundReturn = lcsv[i].FundReturn;
		if(CnGetLen(FundName) > 16) {
			FundName = CnSubString(FundName, 15, true);
		}

		//以下为创建tableview中显示的标签
		var lblFundname = wing.ui.createLabel({
			text : FundName,
			color : 'black',
			top : 7,
			left : 5,
			height : 12,
			font : {
				fontWeight : '',
				fontSize : 12
			},
			clickname : FundCode
		});

		var lblFundcode = wing.ui.createLabel({
			text : FundCode,
			color : 'black',
			top : 25,
			left : 5,
			height : 12,
			font : {
				fontWeight : '',
				fontSize : 12
			},
			clickname : FundCode
		});

		var lblFundnet = wing.ui.createLabel({
			text : FundNet,
			shownumber : true,
			top : 5,
			left : 120,
			height : 20,
			font : {
				fontWeight : 'bold',
				fontSize : 20
			},
			clickname : FundCode
		});

		var lblFunddate = wing.ui.createLabel({
			text : LocalFundFileDate,
			color : 'gray',
			top : 28,
			left : 124,
			height : 10,
			font : {
				fontWeight : '',
				fontSize : 10
			},
			clickname : FundCode
		});

		var lblFundreturn = wing.ui.createStockLabel({
			text : FundReturn,
			colorlabel : true,
			top : 12,
			left : 210,
			height : 20,
			font : {
				fontWeight : 'bold',
				fontSize : 20
			},
			clickname : FundCode
		});

		var rowdata = Ti.UI.createTableViewRow();
		rowdata.add(lblFundname);
		rowdata.add(lblFundcode);
		rowdata.add(lblFundnet);
		rowdata.add(lblFunddate);
		//2011.10.8. #197 基金列表页，基金数据日期没有显示
		rowdata.add(lblFundreturn);
		rowdata.height=43;
		rowdata.filter = FundName + FundCode;
		rowdata.clickname = FundCode;
		rowdata.hasChild = true;
		rowdata.className = 'ttyfunlist';
		if(i == 0) {
			//如果是显示三个月和六个月回报率排行榜，修改标题显示
			if((FundKind == 10) || (FundKind == 11)) {
				rowdata.header = '基金名称          净值       回报率';
			} else {
				//2012.6.20 #147 关注基金、基金列表和基金内页，对于日涨幅数据的描述不统一
				rowdata.header = '基金名称          净值       日涨幅';
			}
		}

		//追加row到tableview中
		tableviewFundList.appendRow(rowdata);
	}
}

//tableviewFundList.data=data;

//tableview基金列表点击事件
tableviewFundList.addEventListener('click', function(e) {
	//点击的基金代码
	var UserFundCode;

	Ti.API.info("clicked on table view = " + e.source);
	Ti.API.info("clicked on row index = " + e.index);

	UserFundCode = e.source.clickname;

	Ti.API.info('clicked Fundcode:' + UserFundCode);

	//防止用户点击最后一行，显示错误基金代码
	if(UserFundCode != 'loading') {

		Titanium.App.Properties.setString('UserFundCode', UserFundCode);

		var winFundDetail = Titanium.UI.createWindow({
			url : 'tabFundDetail.js',
			title : e.rowData.title
		});

		Titanium.UI.currentTab.open(winFundDetail, {
			animated : true
		});
	};
});
// add tableview to the window
win.add(tableviewFundList);

//#230 app crash, 可能是内存泄露
win.addEventListener('blur', function(e) {
	Ti.API.info('window lost focus');
	//FundData=null;
});

//2011.6.11. 根据fund数量判断是否要用 读入中。。。 设计
if(FundCount <= my.DisplayRows) {
	TableViewAppendRow(0, FundCount - 1);
	lastRow = FundCount - 1;
} else {
	TableViewAppendRow(0, my.DisplayRows - 1);
	lastRow = my.DisplayRows;
};

Ti.API.info('lastRow:' + lastRow + ' FundCount:' + FundCount);

loadingRow.title = '基金数据读入中...';
loadingRow.color = 'gray';
loadingRow.clickname = 'loading';
loadingRow.height=43;
tableviewFundList.add(loadingRow);

//2012.3.23. #568 基金列表，向下滑动显示更多时候定位修改
function endUpdate() {
	updating = false;

	tableviewFundList.deleteRow(lastRow, {
		animationStyle : Titanium.UI.iPhone.RowAnimationStyle.NONE
	});

	//loading raw data
	if(FundCount < (lastRow + my.DisplayRows - 1)) {
		TableViewAppendRow(lastRow, FundCount - 1);
		lastRow = FundCount - 1;
	} else {
		TableViewAppendRow(lastRow, lastRow + my.DisplayRows - 1);
		lastRow += my.DisplayRows;
	}

	Ti.API.info('curr last row:' + my.currLastRow);
	tableviewFundList.scrollToIndex(my.currLastRow + 1);

	// just scroll down a bit to the new rows to bring them into view
	//	tableviewFundList.scrollToIndex(lastRow-10, {
	//		animated:true,
	//		position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM
	//	});

	navActInd.hide();

}

//2012.3.23. #568 基金列表，向下滑动显示更多时候定位修改
function beginUpdate() {
	updating = true;

	win.setRightNavButton(navActInd);
	navActInd.show();

	my.currLastRow = tableviewFundList.data[0].rowCount;
	Ti.API.info('curr last row:' + my.currLastRow);

	tableviewFundList.appendRow(loadingRow);

	// just mock out the reload
	setTimeout(endUpdate, 2000);
}

//2011.6.11. edit from example
tableviewFundList.addEventListener('scroll', function(e) {
	//   Ti.API.info('tableview scroll event fired');
	//   Ti.API.info('lastRow:'+lastRow+' FundCount:'+FundCount);

	//如果lastrow=基金数量，说明不需要再 读入。。。
	if(lastRow == (FundCount - 1)) {
	} else {

		var offset = e.contentOffset.y;
		var height = e.size.height;
		var total = offset + height;
		var theEnd = e.contentSize.height;
		var distance = theEnd - total;

		// going down is the only time we dynamically load,
		// going up we can safely ignore -- note here that
		// the values will be negative so we do the opposite
		if(distance < lastDistance) {
			// adjust the % of rows scrolled before we decide to start fetching
			var nearEnd = theEnd * 0.75;

			if(!updating && (total >= nearEnd)) {
				beginUpdate();
			}
		}
		lastDistance = distance;
	}
});

