/*

 FileName: tabChinaIndex.js
 Function:显示国内主要证券指数

 2011.12.14. #374 不联网用存盘的数组
 2011.12.12. #381 增加支持指数显示 沪深300 指数
 2011.12.11. #376 增加刷新数据按钮，调整程序流程
 2011.12.10. create #373
 2012.1.6. #454 bug fix
 2012.1.10. #459 bug fix
 2012.7.3.	#182 理财工具，国内证券指数行情，读取天天盈网站接口避免出现乱码
 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
 */
Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');
var win = Titanium.UI.currentWindow;
win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);
//
var my = {};
// tableview object
var tableviewIndex;
var IndexData, IndexDate, IndexName, Index, IndexChange;
var lblUpdate = wing.ui.createLabel({
	color : 'white',
	backgroundColor : 'transparent',
	bottom : 10,
	left : 60,
	width : 300,
	height : 30
});
var tableviewCreate = false;
win.add(lblUpdate);
//2011.12.14.
//2012.1.10. #459
//显示证券指数
function ShowIndex() {
	try {
		Ti.API.info('stock: ' + IndexData);
		my.data = [];
		for(var i = 0; i < 5; i++) {
			//大盘名称
			my.data.push(IndexData[i]["index_name"]);
			//大盘点数
			my.data.push(formatNumber(IndexData[i]["index_value"], '#,###.000'));
			//涨跌值
			my.data.push(IndexData[i]["index_change"]);
		}
		//由于只显示一个日期，取第一个日期
		IndexDate = IndexData[0]["index_time"];
		//指数数据存盘
		Titanium.App.Properties.setList('IndexData', IndexData);
		//#459
		if(tableviewCreate) {
			win.remove(tableviewIndex)
		};
		//2011.12.27. 修改为配置文件模式
		//2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy		
		tableviewIndex = wing.ui.createTableView({
			type : tableviewLabels,
			value : my.data,
			ui_filename : 'tabChinaIndex.ui'
		})
		tableviewCreate = true;
		lblUpdate.text = '更新时间：' + IndexDate;
		win.add(tableviewIndex);

	} catch(e) {
		ShowInfo('显示指数时发生错误，请检查互联网链接')
	}
	//2011.12.11. create #379
	//tableview指数列表点击事件
	tableviewIndex.addEventListener('click', function(e) {
		if(e.index < 5) {
			Titanium.App.Properties.setInt('IndexRowIndex', e.index)
			var winIndexDetail = Titanium.UI.createWindow({
				url : 'tabIndexDetail.js',
				title : e.rowData.WinTitle
			});
			Titanium.UI.currentTab.open(winIndexDetail, {
				animated : true
			});
		};
	});
}
my.xhr = Titanium.Network.createHTTPClient();
my.xhr.onload = function() {
	Ti.API.info(my.xhr.responseText);
	var response = JSON.parse(my.xhr.responseText);
	if(response.return_code == 1) {
		IndexData = response.index;
		ShowIndex();
	}
};
my.xhr.onerror = function() {
	Ti.API.info('xhr error.');
};
// #376 create
//刷新数据按钮
var btnRefreshData = Titanium.UI.createButton({
	systemButton : Titanium.UI.iPhone.SystemButton.REFRESH
});
win.rightNavButton = btnRefreshData;
//2011.12.14. 判断是否联网 #374 不联网用存盘的数组
//2011.12.
btnRefreshData.addEventListener('click', function(e) {
	Ti.API.info('index refresh click');
	//如果联网状态
	if(CheckInternet(false) == true) {
		my.url = 'http://www.ttyfund.com/api/services/StockService.php?key=TTYFUND-CHINAPNR&act=GetChinaIndex';
		my.xhr.settimeout = 3000;
		my.xhr.open('GET', my.url);
		my.xhr.send();
	} else {
		IndexData = Titanium.App.Properties.getList('IndexData');
		Ti.API.info('loading catch data');
		ShowIndex();
	}
});
//2011.12.10. #372 显示方式调整
//2011.12.10. #371 修改上证指数获得方式
//显示指数
btnRefreshData.fireEvent('click');
//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
