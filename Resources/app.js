/*

 2011.12.20. #407 界面，我的基金 调整为 关注基金
 ----
 //2011.4.18. edit
 //2011.6.19. add welcome
 //2011.6.20. add set portrait
 //2011.6.24. edit tabview
 //2011.6.28. edit tabview
 2012.6.18. ac #137 欢迎页面，出现空白页才显示首页
 2012.6.22 本地存放fundlist9,10 预处理fundlist9
 */

// test git 
// test git again	

Ti.include('urbanairship.js');
Ti.include('/tty_tools/consts.js');
Ti.include('/tools/wing_ui.js');
Ti.include('/tools/tools.js');

Titanium.UI.setBackgroundColor('#000');

Titanium.UI.orientation = Titanium.UI.PORTRAIT;

//  2012.6.22 本地存放fundlist9,10 预处理fundlist9
//检查是否已经生成过funddata
FundData = Titanium.App.Properties.getList('FundData', '');
if(FundData == '')
//本地没有生成过funddata，使用内置的fundlist9
{
	//存储基金基础数据数组
	var FundData0 = [];
	var f;
	var contents;
	f = Titanium.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'data/fundlist9.csv');
	contents = f.read.blob;

	Ti.API.info('file = ' + f);
	Ti.API.info('f = ' + f.exists());
	var lcsv = CSVToArray(contents.text, ';');

	var lFundCount = lcsv.length;
	var lFundCount0 = lFundCount - 1;

	Ti.API.info('FundCount:' + lFundCount);

	var FundCode, FundNet, FundReturn, FundName, FundKind, FundCompanyId;
	var i;

	for( i = 0; i < lFundCount0; i++) {
		FundCode = lcsv[i][0];
		FundNet = lcsv[i][2];
		FundReturn = lcsv[i][3];
		FundName = lcsv[i][1];
		FundKind = lcsv[i][4];
		//增加基金公司id字段
		FundCompanyId = Trim(lcsv[i][5]);
		FundData0.push({
			title : FundCode + ' ' + FundName,
			hasChild : true,
			filter : FundCode + ' ' + FundName,
			clickname : FundCode,
			FundCode : FundCode,
			FundName : FundName,
			FundNet : FundNet,
			FundReturn : FundReturn,
			FundKind : FundKind,
			FundCompanyId : FundCompanyId,
			className : 'fund'
		});
	}
	Titanium.App.Properties.setList('FundData', FundData0);
	Ti.API.info('Funddata array save ok');
} else {
	Ti.API.info('Funddata array already exists');
}
// create tab group
var tabGroup = Titanium.UI.createTabGroup();
//
// create base UI tab and root window
//
// 2012.6.18. ac #137 欢迎页面，出现空白页才显示首页
var win1 = Titanium.UI.createWindow({
	/*2012.6.24 因实机空白太久，改回原来的样式测试
	 *2012.6.24 可能是平台分析功能的问题，换回最新版测试
	 */
	url : 'main/tabTTY.js',
	title : '天天盈基金理财',
	backgroundColor : '#fff',
	barColor : 'faa61a'
});
var tab1 = Titanium.UI.createTab({
	icon : 'images/buttons/tab_tty.png',
	title : '天天盈',
	window : win1
});
win1.hideNavBar();
//2012.5.18. #92 修改到news路径
var win2 = Titanium.UI.createWindow({
	url : 'main/news/tabNews.js',
	title : '基金资讯',
	backgroundColor : '#fff'
});
var tab2 = Titanium.UI.createTab({
	icon : 'images/buttons/tab_news.png',
	title : '基金资讯',
	window : win2
});

var win3 = Titanium.UI.createWindow({
	url : 'main/funds/tabUD.js',
	title : '关注基金',
	backgroundColor : '#fff'
});
var tab3 = Titanium.UI.createTab({
	icon : 'images/buttons/tab_star.png',
	title : '关注基金',
	window : win3
});

var win4 = Titanium.UI.createWindow({
	url : 'main/funds/tabFund.js',
	title : '基金净值',
	backgroundColor : '#fff'
});
var tab4 = Titanium.UI.createTab({
	icon : 'images/buttons/tab_fund.png',
	title : '基金净值',
	window : win4
});

var win5 = Titanium.UI.createWindow({
	url : 'main/tools/tabTools.js',
	title : '理财工具',
	backgroundColor : '#fff'
});
var tab5 = Titanium.UI.createTab({
	icon : 'images/buttons/tab_tool.png',
	title : '理财工具',
	window : win5
});

//
//  add tabs
//
tabGroup.addTab(tab1);
tabGroup.addTab(tab2);
tabGroup.addTab(tab3);
tabGroup.addTab(tab4);
tabGroup.addTab(tab5);

tabGroup.setActiveTab(tab1);

Titanium.App.Properties.setBool('UD_NeedRefresh', true);
Titanium.App.Properties.setString('IndexDate', null);

// open tab group
tabGroup.open({
	transition : Titanium.UI.iPhone.AnimationStyle.CURL_UP
});

//test cloud

//var Cloud = require('/modules/ti.cloud');
//Cloud.debug = true;  // optional; if you add this line, set it to false for production
//Cloud.useSecure =true;

/*
Cloud.Users.create({
username: 'david3',
password: 'password',
password_confirmation: 'password',
first_name: 'first',
last_name: 'last'
}, function (e) {
if (e.success) {
Ti.API.info('create user on cloud ok')
} else {
Ti.API.info('create user on cloud error')
}
});

*/

/*
Cloud.KeyValues.set({
name: 'testkv',
value: 'that ok'
}, function (e) {
if (e.success) {
alert('Success');
} else {
alert('Error:\\n' +
((e.error && e.message) || JSON.stringify(e)));
}
});

*/
//test cloud end

//Ti.API.info('str:'+RemoveAllSpace('12  34'));

// test for loading in a root-level include
//Ti.include("welcome.js");

//urbanship_init();

//test urbansip
//2012.3.
function urbanship_init() {

	//test key begin
	UrbanAirship.key = 'mhXbcK2TQY--pCKJigwzQw';
	UrbanAirship.secret = '9y_ykzytRimoqLEUJDD6wQ';
	UrbanAirship.master_secret = 'SGsFvbhTR7WKRo9ssYw2sg';
	//test key end

	//product key begin
	//      UrbanAirship.key = 'KwL83Mz5R8GBLuWimQQ3bA';
	//      UrbanAirship.secret = 'GsQpHPmqSv6eDNZKrpzTnw';
	//      UrbanAirship.master_secret = '09a5VqnzRbSfDi568qiEYQ';
	//product key end

	UrbanAirship.baseurl = 'https://go.urbanairship.com';

	Ti.Network.registerForPushNotifications({
		types : [Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND],
		success : function(e) {
			var deviceToken = e.deviceToken;
			Ti.API.info('successfully registered for apple device token with ' + e.deviceToken);
			var params = {
				tags : [app_name, app_version, Titanium.Platform.model],
				alias : Titanium.Platform.macaddress//alias.value
			};
			Ti.API.info('ua params:' + params);
			UrbanAirship.register(params, function(data) {
				Ti.API.debug("registerUrban success: " + JSON.stringify(data));
			}, function(errorregistration) {
				Ti.API.warn("Couldn't register for Urban Airship");
			});
		},
		error : function(e) {
			Ti.API.warn("push notifications disabled: " + e);
		},
		callback : function(e) {
			Ti.API.info('push:' + e.data.alert);
			Ti.API.info('push type:' + e.data.type);
			Ti.API.info('push id:' + e.data.id);

			//如果是text的广告
			if(e.data.type == 'text_ad') {
				var alert = Ti.UI.createAlertDialog({
					title : '天天盈理财',
					message : e.data.alert,
					ok : '我知道了'
				});
				alert.show();
			}
			//如果是web广告
			if(e.data.type == 'web_ad') {
				//setup mark to let tabTTY show the web ad
				Titanium.App.Properties.setString('push_show', 'web_ad');
			}
		}
	})
}

/*
 * push json sample""
 * {"aps": {"alert": "测试","type":"text_ad","id":"20120001"}, "device_tokens": ["9F76DA6556B8B437F915591D9731E14AC74C2F32D78086B75003E7ACC356F3AF "]}
 * {"aps": {"alert": "金鹰持久回报分级 债券型证券投资基金 约定年收益4.6% 3月5日-3月16日公开募集，请联系天天盈热线 400-821-3999 或登陆天天盈网站 [汇付天下]"}, "device_tokens": ["933AFE7CF1B8C70567F0CC5EFD484C633355BEF6D36A60AA7A12802BBC2B7DBE"]}
 */