/*

 2011.6.25.
 2011.6.30. add setup button
 2011.7.1. re-arrange UI
 2011.7.13. 美化UI等
 2011.10.4. #164 每次启动app时候增加用户行为访问记录
 2011.10.6. #193 自定义基金，窗口bar颜色修改为标准橙色
 2011.10.8. #241 修改PostBE 调用方式
 2011.11.1. #281 设置按钮从主窗口移动到理财工具
 2011.11.11. #315 “天天盈用户登陆”按钮美化
 2011.11.26. #354 调整调用库函数方式
 2012.2.22. #512 增加检查是否有新版本
 2012.3.4. #524 每次窗口激活操作postbe
 2012.3.9. #534 检查更新，调整为版本数据检查
 2012.3.14. #559 界面，首页天天盈三个调用web功能都显示关闭按钮
 2012.6.7. ac #99: 系统，检查版本更新错误修正
 2012.6.7. ac #71 什么是天天盈，url地址更换
 2012.6.18. ac #138 天天盈首页，点击按钮后如在无网络情况下显示提醒
 * */

Ti.include('../tools/tools.js');
Ti.include('../tools/wing_ui.js');
Ti.include('../tty_tools/consts.js');
Ti.include('../tty_tools/tty_tools.js');

var win = Titanium.UI.currentWindow;
//2011.10.6. #193 自定义基金，窗口bar颜色修改为标准橙色
win.barColor = 'faa61a';

//2011.11.1.
win.hideNavBar();

var my = {};

var pic;

var f = Ti.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, '/images/system/welcome.png');

var imageView = Titanium.UI.createImageView({
	image : f,
	top : -50,
	width : 320
});

var btnWebAD = Titanium.UI.createButton({
	title : '天天盈最新活动',
	height : 40,
	width : 140,
	font : {
		fontSize : 16,
		fontFamily : 'Arial'
	},
	borderRadius : 10,
	borderWidth : 2,
	borderColor : 'gray',
	backgroundColor : '#faa61a',
	backgroundImage : 'NONE',
	selectedColor : 'red',
	top : 400,
	left : 20
});

btnWebAD.addEventListener('click', function(e) {
// 2012.6.18. ac #138 天天盈首页，点击按钮后如在无网络情况下显示提醒	
	CheckInternet();
	var web = wing.ui.createWeb({
		Url : 'http://www.ttyfund.com/m/ad/20120001/',
		Title : '天天盈',
		CloseNav : true,
		BarColor : 'faa61a',
		ShowClose:true
	});
});



// show 介绍 button
var btnIntro = Titanium.UI.createButton({
	title : '什么是天天盈',
	height : 40,
	width : 120,
	font : {
		fontSize : 16,
		fontFamily : 'Arial'
	},
	borderRadius : 10,
	borderWidth : 2,
	borderColor : 'gray',
	backgroundColor : '#faa61a',
	backgroundImage : 'NONE',
	selectedColor : 'red',
	top : 400,
	left : 180
});

//2012.3.14. #559 界面，首页天天盈三个调用web功能都显示关闭按钮
btnIntro.addEventListener('click', function(e) {
// 2012.6.18. ac #138 天天盈首页，点击按钮后如在无网络情况下显示提醒	
	CheckInternet();	
	var web = wing.ui.createWeb({
		// 2012.6.7. ac #71 什么是天天盈，url地址更换
		Url : 'https://account.ttyfund.com/m/tty.html',
		Title : '什么是天天盈',
		CloseNav : false,
		BarColor : 'faa61a',
		ShowClose:true
	})
});



// show register button
//2011.11.11. #315 “天天盈用户登陆”按钮美化
//2012.3.14. #559 界面，首页天天盈三个调用web功能都显示关闭按钮
var btnLogin = Titanium.UI.createButton({
	title : '天天盈用户登录',
	height : 40,
	width : 140,
	font : {
		fontSize : 16,
		fontFamily : 'Arial'
	},
	borderRadius : 10,
	borderWidth : 2,
	borderColor : 'gray',
	backgroundColor : '#f37021',
	backgroundImage : 'NONE',
	selectedColor : 'red',
	top : 300,
	left : 90
});

//2011.11.1. 天天盈用户登陆按钮
//2012.2.14. 测试交易接口
//2012.3.14. #559 界面，首页天天盈三个调用web功能都显示关闭按钮
btnLogin.addEventListener('click', function(e) {
// 2012.6.18. ac #138 天天盈首页，点击按钮后如在无网络情况下显示提醒	
	CheckInternet();
	var web = wing.ui.createWeb({
		Url : 'https://account.ttyfund.com/iphone/tty_mobile/tty_mobile/page/signin/login.php',
		//Url : 'https://account.ttyfund.com/iphone/iphonetest/tty_mobile_trade/page/login/login.php',
		Title : '天天盈',
		CloseNav : true,
		BarColor : 'faa61a',
		ShowClose:true
	});
});

win.startLayout()
win.add(imageView);

imageView.startLayout();

imageView.add(btnWebAD);
imageView.add(btnIntro);
imageView.add(btnLogin);

imageView.finishLayout();

win.finishLayout();



//2012.3.4. #523 系统，检查更新功能独立成一个函数
//2012.3.9. #534 检查更新，调整为版本数据检查
//检查是否有更新版本，提示用户
function CheckUpdate() {
	//check update
	var needCheckUpdate = Titanium.App.Properties.getBool('needCheckUpdate', true);
	Ti.API.info('isUpdate:' + needCheckUpdate);
	if(needCheckUpdate) {
		if(CheckInternet(false)) {

			my.xhr = Ti.Network.createHTTPClient();
			var remoteUrl = 'http://www.ttyfund.com/api/mobile/tty-client_ios.txt';
			my.xhr.open("GET", remoteUrl);
			var json = {};

			my.xhr.onload = function() {
				try {
					json = JSON.parse(my.xhr.responseText);
					Ti.API.info('remote version:' + json.version);
					Ti.API.info('remote version_number:' + json.version_number);
					Ti.API.info('app url:' + json.url);

					if(app_version_number >= json.version_number) {

					} else {
						updateDialog = Ti.UI.createAlertDialog({
							//2012.6.7. ac #99: 系统，检查版本更新错误修正
							title : '天天盈客户端v' + json.version + '发布了！',
							message : ' ',
							buttonNames : ['立即升级', '取消', '不再提醒'],
						});
						updateDialog.addEventListener('click', function(e) {
							if(Ti.Platform.osname === 'android' && a.buttonNames === null) {
								alert('There was no button to click');
							} else {
								var num = e.index;
								if(num == 0)//open appstore
								{
									Ti.Platform.openURL(json.url);
								} else if(num == 1)//do nothing
								{

								} else if(num == 2)//set checkUpdate=false
								{
									Titanium.App.Properties.setString('needCheckUpdate', false);
								}
								Ti.API.info('clicked:' + num);
							}
						});

						updateDialog.show();
					}
				} catch(E) {
					Ti.API.info(E);
				}
			};
			my.xhr.send();
		}
	}
}

//第一次执行时候检查是否有新版本
CheckUpdate();


//2012.3.4. #524 每次窗口激活操作postbe
win.addEventListener('focus', function(e) {
	setTimeout(function() {
		PostBE(app_name + app_version);
	}, 2000);
});
//#527,根据push显示web广告内容
function ShowPushWebAd() {

	if(Titanium.App.Properties.getString('push_show') == 'web_ad') {

		Ti.API.info('found push web_ad mark');
		Titanium.App.Properties.setString('push_show', '');

		var viewAD = Ti.UI.createView({
			backgroundColor : 'transparent'
		});

		var web = Ti.UI.createWebView({
			url : web_ad_url
		});

		var actInd = Titanium.UI.createActivityIndicator({
			message : '正在读入',
			height : 50,
			width : 10,
			style : Titanium.UI.iPhone.ActivityIndicatorStyle.DARK
		});

		web.addEventListener('beforeload', function(e) {
			Ti.API.info('begin to load');
			Ti.API.info('web url:' + web_ad_url);
			actInd.show();
		});
		web.addEventListener('load', function(e) {
			Ti.API.info('load ok');
			actInd.hide();
		});
		//create close button
		var btnClose = Ti.UI.createButton({
			title : '关闭',
			height : 30,
			width : 80,
			font : {
				fontsize : 12
			},
			borderColor : 'white',
			backgroundColor : '#f37021',
			backgroundImage : 'NONE',
			selectedColor : 'blue',
			bottom : 5,
			left : 120
		});

		btnClose.addEventListener('click', function(e) {
			win.remove(viewAD);
		});
		viewAD.add(web);
		viewAD.add(btnClose);
		win.add(viewAD);

	} else {
		Ti.API.info('not found push web_ad mark');
	}
}