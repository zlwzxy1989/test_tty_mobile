/*

 2012.2.5. 关注基金修改为单独窗口编辑
 2012.2.11. #502 关注基金，编辑基金窗口修改过内容后回到关注基金界面需要刷新
 2012.2.12. #503 关注基金功能相关调整
 2012.5.9. ac #75 关注基金，编辑状态下基金代码下留白,调整row告诉和label高度解决

 */
Ti.API.info('now...');
Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;
win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

Ti.API.info(win.backgroundImage);

Titanium.App.Properties.setBool('UDEdit', true);

var UD_FundCount;
var UD_FundCode = [];

var  FundCode, FundNet, FundReturn, FundName, FundCount;

//#183 自定义基金，编辑时候，删除按钮显示中文“删除”
//2011.10.1.
var tableviewUD = Titanium.UI.createTableView({
	deleteButtonTitle : '删除',
	editable : true,
	moveable : true,
	backgroundColor : 'transparent',
	style : Titanium.UI.iPhone.TableViewStyle.GROUPED
});

var FundData = [];

//从总体数组FundData中获得指定fundcode的fund基础信息
//2011.
function GetFundDetail(lFundCode) {

	var i = 0;
	var b = true;

	while(b) {
		//控制循环
		if(i == (FundData.length)) {
			b = false;
		} else {

			if(lFundCode == FundData[i].FundCode) {
				FundCode = lFundCode;
				FundName = FundData[i].FundName;
				FundNet = FundData[i].FundNet;
				FundReturn = FundData[i].FundReturn;
				b = false;
			}
			i++;
		}
	}
}

//2012.2.12. 从tabUD中移植
//#503 关注基金功能相关调整
function RefreshUD() {
	Ti.API.info('UD Edit RefreshUD executing…');
	FundData = Titanium.App.Properties.getList('FundData');

	//获得用户自定义基金数量
	UD_FundCount = Titanium.App.Properties.getInt('UD_FundCount');
	Ti.API.info('User Define Fund Count is:' + UD_FundCount);

	//读出用户自定义数组
	UD_FundCode = Titanium.App.Properties.getList('UD_FundCode');

	Ti.API.info('UD_FundCode:' + UD_FundCode);

	tableviewUD.data = null;
	var rowdata = [];
	for( i = 0; i < UD_FundCount; i++) {
		Ti.API.info('ud ' + UD_FundCode[i]);

		//根据用户自定义FundCode获得该基金的详细信息
		GetFundDetail(UD_FundCode[i]);

		//判断基金名称是否过长需要截断显示
		if(CnGetLen(FundName) > 32) {
			FundName = CnSubString(FundName, 15, true);
		}

		//以下为创建tableview中显示的标签
		var lblFundname = wing.ui.createLabel({
			text : FundName,
			color : 'black',
			top : 7,
			left : 5,
			height : 14,
			font : {
				fontWeight : '',
				fontSize : 14
			},
			clickname : FundCode
		});

		var lblFundcode = wing.ui.createLabel({
			text : FundCode,
			color : 'black',
			top : 25,
			left : 5,
			height : 12,
			font : {
				fontWeight : '',
				fontSize : 12
			},
			clickname : FundCode
		});

		var row = Ti.UI.createTableViewRow();
		row.height=43;
		row.add(lblFundname);
		row.add(lblFundcode);
		row.clickname = FundCode;
		row.backgroundColor = 'white';

		//显示header
		if(i == 0) {
			row.header = '基金名称';
		}

		rowdata.push(row);
	}

	tableviewUD.data = rowdata;

	tableviewUD.editing = true;
}

//2011.10.8. #196 自定义基金，调整基金显示顺序自动保存，如果发生删除等操作
//add delete event listener
//2011.6.25.
tableviewUD.addEventListener('delete', function(e) {
	SaveUD();
	ShowInfo('已经从关注基金中删除');
	Titanium.App.Properties.setBool('UDEdit', false);
});

win.add(tableviewUD);

//2012.2.12. 从tabUD中移植
//#503 关注基金功能相关调整
//#510 delete后会出发win focus，内部delete后不进行refresh
win.addEventListener('focus', function(e) {
	Ti.API.info('win focus');
	if (Titanium.App.Properties.getBool('UDEdit')){
		RefreshUD()	
	}	
});

win.addEventListener('blur', function(e) {
	Ti.API.info('win blur');	
});

//2011.10.8. #196 自定义基金，调整基金显示顺序自动保存
//2012.2.11. #502 关注基金，编辑基金窗口修改过内容后回到关注基金界面需要刷新
function SaveUD() {

	var i, n, row;
	//捕捉错误，防止最后一个自定义基金删除时候无法获得rowCount
	try {
		n = tableviewUD.data[0].rowCount;
		Ti.API.info('n:' + n);

		for( i = 0; i < n; i++) {
			row = tableviewUD.data[0].rows[i];
			UD_FundCode[i] = row.clickname;
			Ti.API.info('tableview UD ' + i + ':' + row.clickname);
		}
		UD_FundCount = n;

	} catch (e) {
		UD_FundCode = null;
		UD_FundCount = 0;
	}

	//数组和计数器存盘
	Titanium.App.Properties.setList('UD_FundCode', UD_FundCode);
	Titanium.App.Properties.setInt('UD_FundCount', UD_FundCount);

	//设置UD强制刷新
	Titanium.App.Properties.setBool('UD_NeedRefresh', true);
}

var btnOK = Titanium.UI.createButton({
	title : '完成',
	style : Titanium.UI.iPhone.SystemButtonStyle.DONE
});

btnOK.addEventListener('click', function() {
	SaveUD();
	win.close();
});
//#163 在tableview左上“增加”按钮，直接可以查询基金列表
var btnAdd = Titanium.UI.createButton({
	systemButton : Titanium.UI.iPhone.SystemButton.ADD
});

//"增加"按钮点击后处理
btnAdd.addEventListener('click', function() {

	//创建基金列表窗口
	var winFundSearch = Titanium.UI.createWindow({
		url : 'tabFundSearch.js',
		title : '基金搜索'
	});

	Titanium.UI.currentTab.open(winFundSearch, {
		animated : true
	});
});
win.setRightNavButton(btnOK);
win.setLeftNavButton(btnAdd);
//#163 end

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});