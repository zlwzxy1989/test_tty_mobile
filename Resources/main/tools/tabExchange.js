/*

 FileName: tabExchange.js
 Function:显示主要汇率

 2011.12.14. create
 2011.12.16. 修改优化
 2011.12.17. #396 外汇，对于金额输入用数字正则进行判断，不对的话进行提示
 2011.12.19. #402 外汇，保存用户上次使用时选择的外汇币种
 2011.12.23. 修复各类小错误
 2012.1.7. #403 交换from和to货币币种按钮
 2012.2.16. #508

 * */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');
Ti.include('../../tools/suds.js');

//
var my = {};
my.money = 0;
my.data = [];
//货币数组
my.currency = ['人民币', '美元', '港币', '澳元', '加元', '欧元', '日元', '韩元', '新西兰元', '英镑', '新加坡元', '台币'];
//货币对应代码数组
my.cur_sc = ['CNY', 'USD', 'HKD', 'AUD', 'CAD', 'EUR', 'JPY', 'KRW', 'NZD', 'GBP', 'SGD', 'TWD'];
//主窗口
var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

var lblHint = wing.ui.createLabel({
	text : '如果不能显示结果，可能是外汇实时接口忙碌，请15分钟左右后再尝试',
	color : 'darkgray',
	backgroundColor : 'transparent',
	font : {
		fontsize : 12
	},
	bottom : 80
});

win.add(lblHint);

//开始创建主tableview
function SetupTableView() {

	my.data = [];

	if(Titanium.App.Properties.getString('Exchange_From') === null) {
		my.lvalue = my.currency[0];
		my.from = my.currency[0];
	} else {
		my.lvalue = Titanium.App.Properties.getString('Exchange_From');
		my.from = my.lvalue;
	}

	//from 货币 row
	row = wing.ui.createTableViewRow({
		type : rowPicker,
		picker_type : pickerSingle,
		picker_data : my.currency,
		title : '基准货币:',
		value : my.lvalue,
		buttonOK_title : '确认'
	});

	//2011.12.16.
	row.addEventListener('RowChange', function(e) {
		//获得用户选择
		my.from = Titanium.App.Properties.getString('RowPicker');
		Ti.API.info('from:' + my.from);
	});
	//设定from的初始值

	my.data[0] = row;

	//to 货币 row
	if(Titanium.App.Properties.getString('Exchange_To') === null) {
		my.lvalue = my.currency[1];
		my.to = my.currency[1];
	} else {
		my.lvalue = Titanium.App.Properties.getString('Exchange_To');
		my.to = my.lvalue;
	}
	row = wing.ui.createTableViewRow({
		type : rowPicker,
		picker_type : pickerSingle,
		picker_data : my.currency,
		title : '转换货币:',
		value : my.lvalue,
		buttonOK_title : '确认'
	});

	//row 内容修改事件
	//2011.12.16.
	row.addEventListener('RowChange', function(e) {
		my.to = Titanium.App.Properties.getString('RowPicker');
		Ti.API.info('to:' + my.to);
	});
	//设定to的初始值

	my.data[1] = row;

	//输入金额 row
	row = wing.ui.createTableViewRow({
		type : rowText,
		keyboard : keyboardNumbers,
		title : '金额:',
		value : '0',
		textAlign : 'right',
		width : 300
	});

	//row 内容修改事件
	//2011.12.16.
	row.addEventListener('RowChange', function(e) {
		my.money = Titanium.App.Properties.getString('RowText');
		//my.money=RemoveAllSpace(my.money);
	});
	my.data[2] = row;

	row.addEventListener('RowFocus', function(e) {
		Ti.API.info('row focus');
		//win.add(viewShadow);
	});

	row.addEventListener('RowLostFocus', function(e) {
		Ti.API.info('row lost focus');
		//win.remove(viewShadow);
	});
}

SetupTableView();

// tableview object
var tableviewExchange = Titanium.UI.createTableView({
	backgroundColor : 'transparent',
	data : my.data,
	style : Titanium.UI.iPhone.TableViewStyle.GROUPED
});

win.add(tableviewExchange);

//计算货币汇率按钮
var btnCalExchange = wing.ui.createButton({
	title : '计算',
	bottom : 5,
	left : 10,
	color : 'black',
	type : buttonGlass,
	buttonGlassColor : buttonGlassYellow,
	dir_level:2
});

//2011.12.19. edit
//计算汇率
btnCalExchange.addEventListener('click', function(e) {

	var lCheckMark = true;

	//#402 外汇，保存用户上次使用时选择的外汇币种
	//记录用户所选
	Titanium.App.Properties.setString('Exchange_From', my.from);
	Titanium.App.Properties.setString('Exchange_To', my.to);

	//#396 外汇，对于金额输入用数字正则进行判断，不对的话进行提示
	//检查金额是否输入合法
	if(isRegExp(regFloat, my.money) == false) {
		ShowInfo('金额输入错误，请重新输入');
		lCheckMark = false;
	}

	if(lCheckMark) {
		if(my.money == 0) {
			ShowInfo('金额为零，请重新输入');
			lCheckMark = false;
		}
	}

	if(lCheckMark) {
		if(my.from == my.to) {
			ShowInfo('基准货币和转换货币一样，请重新选择');
			lCheckMark = false;
		}
	}

	//检查错误标识
	if(lCheckMark == false) {
	} else {

		tableviewExchange.touchEnabled = false;
		btnCalExchange.touchEnabled = false;

		var actInd = Titanium.UI.createActivityIndicator({
			height : 50,
			width : 10,
			style : Titanium.UI.iPhone.ActivityIndicatorStyle.DARK
		});
		win.add(actInd);
		actInd.show();
		//	setTimeout(function() {

		var callparams = {
			FromCurrency : my.cur_sc[my.currency.findIndex(my.from)],
			ToCurrency : my.cur_sc[my.currency.findIndex(my.to)]
		};
		Ti.API.info('my from and to:' + my.from + ' ' + my.to);
		Ti.API.info('my from index:' + my.currency.findIndex(my.from));

		Ti.API.info(callparams);

		//建立soap对象
		var suds = new SudsClient({
			endpoint : 'http://www.webservicex.net/CurrencyConvertor.asmx',
			targetNamespace : 'http://www.webserviceX.NET/'
		});

		try {
			//调用soap
			suds.invoke('ConversionRate', callparams, function(xmlDoc) {
				var results = xmlDoc.documentElement.getElementsByTagName('ConversionRateResult');
				if(results && results.length > 0) {
					var result = results.item(0);
					var r1 = results.item(0).text;
					//for memory
					Ti.API.info('1 Euro buys you ' + r1 * my.money + ' U.S. Dollars.');
					//创建结果row
					row = wing.ui.createTableViewRow({
						type : rowText,
						title : '结果:',
						value : formatNumber(r1 * my.money, '#,###.00'),
						textAlign : 'right',
						width : 300,
						editable : false
					});
					my.data[3] = row;

					//创建显示汇率row
					row = wing.ui.createTableViewRow({
						type : rowText,
						title : '汇率:',
						value : formatNumber(r1, '#,###.00'),
						textAlign : 'right',
						width : 300,
						editable : false
					});
					my.data[4] = row;

					//刷新 tableview
					tableviewExchange.setData(my.data);

				} else {
					Ti.API.info('Oops, could not determine result of SOAP call.');
				}
			});
		} catch(e) {
			Ti.API.error('Error: ' + e);
			ShowInfo('在读取汇率时发生错误，请稍后再试');
		}
		tableviewExchange.touchEnabled = true;
		btnCalExchange.touchEnabled = true;

		actInd.hide();

	}
});
//#403 交换from和to货币币种
win.add(btnCalExchange);

//2012.1.7.
var btnSwitchCur = Titanium.UI.createButton({
	image : '../../images/system/btn_change_cur.png'
});

//2012.2.16. edit #508
btnSwitchCur.addEventListener('click', function(e) {

	Titanium.App.Properties.setString('Exchange_From', my.to);
	Titanium.App.Properties.setString('Exchange_To', my.from);

	tableviewExchange.data = null;
	SetupTableView();
	tableviewExchange.data = my.data;
	//#508 外汇兑换计算器，互换币种后，金额输入框金额清零，但数据没有清零
	my.money = 0;
});

win.rightNavButton = btnSwitchCur;
//#403 end

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
