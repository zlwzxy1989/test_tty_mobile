/*
 * 2012.6.22 created by zxy
 * 2012.6.22 #154: 基金净值，点击基金公司按钮后，显示基金公司列表，右侧显示A-Z检索功能栏
 * 2012.6.22 #155: 基金净值，基金公司，在基金公司列表中点击某基金公司，进入基金公司基金列表
 * 2012.6.22 #156: 基金净值，基金公司，右侧A-Z检索栏，点击字母则滑动至相应拼音首字母的基金公司一栏
 * */
Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;
win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

var my = {};
//检查是否有下载的基金公司对应表
FundCompanyData = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'fundlist10.csv');
if(FundCompanyData.exists() == false) {
	//没有下载文件，读取本地默认
	Ti.API.info('fundcompany file empty,begin to read default');
	FundCompanyData = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, 'data/fundlist10.csv');
} else {
	//有下载文件，继续处理
	Ti.API.info('fundcompany file exists');
}
contents = FundCompanyData.read.blob;
lcsv = CSVToArray(contents.text, ';');
FundCount = lcsv.length;
//tableView数据数组
my.companyData = [];
//搜索导航
my.index = [];
var TempHeader = '';
for(var i = 0; i < FundCount - 1; i++) {
	//lcsv 0:首字母 1:公司简称 2:公司ID
	//生成tableViewRow
	row = Ti.UI.createTableViewRow({
		title : lcsv[i][1] + '基金公司',
		height : 43,
		width : '100%',
	});

	if(lcsv[i][0] != TempHeader) {
		//首字母变化时
		//记下首字母的变化
		TempHeader = lcsv[i][0];
		//增加tableViewRow的header
		row.header = lcsv[i][0];
		//增加导航数组记录
		my.index.push({
			'title' : TempHeader,
			'index' : i
		});
	}
	my.companyData.push(row);
}
tableViewFundCompany = Ti.UI.createTableView({
	width : '100%',
	data : my.companyData,
	//2012.6.22 #156: 基金净值，基金公司，右侧A-Z检索栏，点击字母则滑动至相应拼音首字母的基金公司一栏
	index : my.index
})
tableViewFundCompany.addEventListener('click', function(e) {
	//2012.6.22 #155: 基金净值，基金公司，在基金公司列表中点击某基金公司，进入基金公司基金列表
	var companyId = Trim(lcsv[e.index][2]);
	Ti.API.info('company id: ' + companyId);
	//记录基金公司id
	Ti.App.Properties.setString('companyId', companyId);
	//决定fundlist的显示形式
	Ti.App.Properties.setString('fundListDisplayType', 'company');
	var w = Titanium.UI.createWindow({
		url : 'tabFundList.js',
		title : lcsv[e.index][1] + '基金'
	});
	Titanium.UI.currentTab.open(w, {
		animated : true
	});
});
win.add(tableViewFundCompany);

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
