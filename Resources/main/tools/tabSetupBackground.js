/*

 设置背景颜色窗口

 2012.2.4. create

 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

var tableviewBackground = wing.ui.createTableView({
	type : tableviewSimple,
	ui_filename : 'tabBackground.ui',
	dir_level:2
})

tableviewBackground.addEventListener('click', function(e) {

	var lindex = e.index + 1;
	Ti.API.info('back:' + lindex);
	win.backgroundImage = '../../images/bg/back0' + lindex + '.jpg';
	Titanium.App.Properties.setInt('User_BackgroundIndex', lindex);

	Ti.App.fireEvent('app:background_changed');

})

win.add(tableviewBackground);
