//2011.6.19.
//2012.4.3. 去除welcome文字特效,本文件不再需要

// use a closure to (a) test it and (b) not expose this into global scope
(function() {
	// window container
	var welcomeWindow = Titanium.UI.createWindow({
		height : 80,
		width : 200,
		touchEnabled : false
	});

	welcomeWindow.open();

	var animationProperties = {
		delay : 1500,
		duration : 1000,
		opacity : 0.1
	};

	animationProperties.transform = Ti.UI.create2DMatrix().translate(-200, 200).scale(0);

	welcomeWindow.animate(animationProperties, function() {
		welcomeWindow.close();
	});
})();
