/*
 2011.2.15. from tuan
 2011.2.16. edit the UI v1.0.1
 2011.3.5. add tabview browse + wordpress, copy from Tuan Project
 2011.3.12. edit the tableview browser code
 2011.3.13. add internet connection check
 2011.3.13. add save picture for browser
 2011.6.8. continue to develope
 2011.6.9. edit news category
 2011.6.10. add browser nav bar
 2011.6.24. 将 基金课堂 移植到资讯一起
 2011.8.11. 基金新闻位置靠前
 2011.10.6. #189 增加背景显示，tableview修改为透明
 2011.10.8. #224 调用browser显示内容的地方增加设置bar背景颜色
 tableView.addEventListener('click',...
 2011.11.26. #354 调整调用库函数方式
 2012.1.1. tableview修改为配置文件模式
 2012.3.4. #524 每次窗口激活操作postbe
 2012.5.3 修改url为分类id
 2012.5.7. 修改新闻读取方式 by zxy
 #61 基金资讯，分为“理财推荐”“基金资讯”“天天盈动态”三个板块
 2012.5.8. #92 修改到news路径
 2012.6.6. ac #111: 基金内页，根据基金所属基金公司读取相关新闻
 2012.6.19 #145: 基金资讯，界面背景色不响应修改背景色功能
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');
Ti.include('../../tty_tools/consts.js');
Ti.include('../../tty_tools/tty_tools.js');

var win = Titanium.UI.currentWindow;
win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

//2012.5.18. #92 add dir_level:2 适应新路径体系
//2012.1.1
var tableviewNews = wing.ui.createTableView({
	type : tableviewSimple,
	ui_filename : 'tabNews.ui',
	rss : true,
	dir_level : 2
})

//add click event  -by zxy
tableviewNews.addEventListener('click', function(e) {
	Ti.API.info('e.index:' + e.index);
	if(e.rowData.url != null) {
		Titanium.App.Properties.setInt('newsCategory', e.rowData.url);
		Titanium.App.Properties.setString('newsCatCN', e.rowData.title);
		//2012.6.6. ac #111: 基金内页，根据基金所属基金公司读取相关新闻
		Titanium.App.Properties.setString('news_display_type', 'normal');
		var win = wing.ui.createWindow({
			url : 'tabNewsList.js',
			title : e.rowData.title,
			barColor : 'faa61a'
		});
		win.backgroundImage = GetBackgroundFile();
		Titanium.UI.currentTab.open(win, {
			animated : true
		});
	}
});
// add table view to the window
win.add(tableviewNews);

//2012.3.4. #524 每次窗口激活操作postbe
win.addEventListener('focus', function(e) {
	setTimeout(function() {
		PostBE(app_name + app_version);
	}, 2000);
})
//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	//2012.6.19 #145: 基金资讯，界面背景色不响应修改背景色功能
	win.backgroundImage = GetBackgroundFile(2);
});
