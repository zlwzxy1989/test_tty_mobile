/*

 2011.6.25. create
 将更新数据文件功能移到这里
 2011.6.27. edit
 2011.6.28. 增加基金数据预处理
 2011.7.7. 增加证券指数显示
 2011.7.18. 指数显示使用控件方式
 2011.8.24. 修改下载csv文件
 2011.10.6. #188 显示证券指数界面调整，字体大小
 function ShowIndex()
 2011.10.6. #191 基金列表，窗口增加背景色
 function ShowIndex() 等
 2011.10.8. #227 基金净值，证券指数和日期标题显示在tableview中
 function ShowIndex()
 以及代码优化
 2011.10.19. #230 设置tableview数据,防止系统错误
 function ShowIndex()
 2011.11.11. #314 POSTBE代码移动到下载数据处，以便精确统计
 2011.11.26. #354 调整调用库函数方式
 2011.12.10. #372 调整证券指数显示方式
 2011.12.20. #408 界面，基金净值界面调整
 2011.12.21. #412 基金列表，xhr读入方式优化
 2011.12.23. #419 基金列表，去除基金名称中的多余空格 function GetFundData()
 2011.12.25. #435 tableview呈现修改为配置文件模式
 2012.3.8. #536 多重路径支持修改
 2012.6.20 #150 基金净值，基金配置型基金改为指数型基金
 2012.6.20 #151 基金净值，列表中指数型基金添加icon
 2012.6.22 #153 基金净值，头部左侧添加基金公司按钮，搜索按钮移至右侧与刷新按钮并列
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');
Ti.include('../../tty_tools/consts.js');
Ti.include('../../tty_tools/tty_tools.js');

var win = Titanium.UI.currentWindow;
win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

Ti.API.info('Platform:' + Titanium.Platform.name);

//2011.12.25. #435 修改为配置文件模式
var tableviewFund = wing.ui.createTableView({
	type : tableviewSimple,
	ui_filename : 'tabFund.ui',
	dir_level : 2
})

CheckInternet();

// ACTIVITY INDICATOR (TOOLBAR)
var toolActInd;
toolActInd = Titanium.UI.createActivityIndicator({
	style : Titanium.UI.iPhone.ActivityIndicatorStyle.PLAIN,
	font : {
		fontFamily : 'Helvetica Neue',
		fontSize : 15,
		fontWeight : 'bold'
	},
	color : 'white'
});

var flexSpace = Titanium.UI.createButton({
	systemButton : Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

//get remote fund list files
//2011.6.14.
function GetRemoteFundList(lFileName) {
	c = Titanium.Network.createHTTPClient();
	c.onload = function(e) {
		Ti.API.info('remote ' + lFileName + ' file ok, ' + e);
	};
	c.ondatastream = function(e) {
		Ti.API.info('ONDATASTREAM1 - PROGRESS: ' + e.progress);
	};
	c.onerror = function(e) {
		Ti.UI.createAlertDialog({
			title : '出错',
			message : 'Error: ' + e.error
		}).show();
	};
	var setup_debug = Titanium.App.Properties.getBool('setup_debug', false);

	//根据debug模式来切换下载路径
	//2012.6.20 改为true用于测试 2012.6.26 改回正式环境
	if(setup_debug == false) {
		c.open('GET', 'http://www.ttyfund.com/api/mobile/api/' + lFileName);
	} else {
		c.open('GET', 'http://www.ttyfund.com/api/mobile/api/debug/' + lFileName);
	}

	if(Titanium.Platform.name == 'iPhone OS') {
		c.file = Titanium.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, lFileName);
	}
	c.send();

}

//2011.12.23. #419 基金列表，去除基金名称中的多余空格
//2011.8.24.
//2011.6.27.6.29.
//生成内部净值数组
function GenFundData() {

	//存储基金基础数据数组
	var FundData0 = [];
	var f;
	var contents;
	f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'fundlist9.csv');
	contents = f.read.blob;

	Ti.API.info('file = ' + f);
	Ti.API.info('f = ' + f.exists());
	var lcsv = CSVToArray(contents.text, ';');

	var lFundCount = lcsv.length;
	var lFundCount0 = lFundCount - 1;

	//Ti.API.info('lcsv:'+lcsv);
	Ti.API.info('FundCount:' + lFundCount);

	var FundCode, FundNet, FundReturn, FundName, FundKind;
	var i;

	for( i = 0; i < lFundCount0; i++) {
		FundCode = lcsv[i][0];
		FundNet = lcsv[i][2];
		FundReturn = lcsv[i][3];
		//#419 基金列表，去除基金名称中的多余空格
		FundName = lcsv[i][1].removeAllSpace();
		FundKind = lcsv[i][4];
		FundCompanyId = Trim(lcsv[i][5]);
		//Ti.API.info(i + ' array FundCode:' + FundCode + ' FundName:' + FundName + ' FundNet:' + FundNet + ' FundKind:' + FundKind);

		FundData0.push({
			title : FundCode + ' ' + FundName,
			hasChild : true,
			filter : FundCode + ' ' + FundName,
			clickname : FundCode,
			FundCode : FundCode,
			FundName : FundName,
			FundNet : FundNet,
			FundReturn : FundReturn,
			FundKind : FundKind,
			FundCompanyId : FundCompanyId,
			className : 'fund'
		});

		//Ti.API.info('FundType:'+FundType);

		//#380 修改读入基金数据时，最后一个数字总是比总数差一
		toolActInd.Message = '基金数据：' + (i + 1) + ':' + lFundCount0;

	}

	//标记已经生成了数组
	Titanium.App.Properties.setBool('GenFundList', true);
	Titanium.App.Properties.setList('FundData', FundData0);
	Ti.API.info('Funddata array save ok');
	//Ti.API.info('funddata:' + FundData0);

	//设置UD需要刷新标识
	//2011.10.1
	Titanium.App.Properties.setBool('UD_NeedRefresh', true);
}

var LocalFundFileDate, RemoteFundFileDate;

var my = {};
// XHR POST
my.xhr = Titanium.Network.createHTTPClient();
var xmlstr, xmlDate, s, j;

//2011.6.14.
//2011.6.18.
my.xhr.onload = function() {
	xmlstr = this.responseText;

	//check local date mark
	LocalFundFileDate = Titanium.App.Properties.getString('LocalFundFileDate');

	//获得远程生成fundlist文件的日期
	xmlDate = Ti.XML.parseString(xmlstr).documentElement;
	Ti.API.info('xml fundnet:' + xmlDate);
	s = xmlDate.getElementsByTagName('date');
	RemoteFundFileDate = s.item(0).text;
	Ti.API.info('Remote Date:' + RemoteFundFileDate);
	Ti.API.info('Local Date:' + LocalFundFileDate);

	//LocalFundFileDate is null, go to get the remote files
	//本地数据文件日期小于远程，需要数据文件更新
	if((LocalFundFileDate == null) || (LocalFundFileDate < RemoteFundFileDate)) {
		Ti.API.info('Begin to download fund file');

		tableviewFund.touchEnabled = false;

		//2011.11.11. #314 代码移动到下载数据处，以便精确统计
		//#164 每次启动app时候增加用户行为访问记录
		// 2011.10.8. #241 修改PostBE 调用方式
		setTimeout(function() {
			PostBE(app_name + app_version);
		}, 2000);

		win.setToolbar([flexSpace, toolActInd, flexSpace], {
			animated : true
		});

		toolActInd.message = '净值数据下载中，请稍等...';
		toolActInd.show();

		//fundlist6 7 8 = topten, fundlist9 is the whole
		for( j = 10; j > 5; j--) {
			GetRemoteFundList('fundlist' + j + '.csv');
			Ti.API.info('fundlist' + j + '.csv ok');
		}

		//#372
		//GetRemoteFundList('fundindex.csv');

		setTimeout(function() {
			//toolActInd.hide();
			tableviewFund.touchEnabled = true;
			toolActInd.Message = '净值数据下载完毕，处理中';

			//生成本地基金数据
			GenFundData();
			win.setToolbar(null, {
				animated : true
			});
		}, GetPhoneTimeout());
		Titanium.App.Properties.setString('LocalFundFileDate', RemoteFundFileDate);
		Ti.API.info('Local Date Reset:' + RemoteFundFileDate);
	} //if end
}

my.xhr.onerror = function() {
	Ti.API.info('xhr error.');
};

//2011.12.21. edit
function RefreshData() {
	my.url = 'http://www.ttyfund.com/api/mobile/api/date.xml';
	my.xhr.settimeout = 3000;
	my.xhr.open('GET', my.url);
	my.xhr.send();
}

//2011.10.9.
//2011.7.5.
win.addEventListener('focus', function(e) {
	RefreshData();
});
// 2011.10.14. #237 基金列表，去除每次点击tablerow检查文件是否存在
// 2011.10.8. #225 基金净值列表设置背景
// create table view event listener
// fundlist 规则 0-7：基金类别  8: 所有基金 9 10 11：三个排行榜
tableviewFund.addEventListener('click', function(e) {

	try {
		//获得点击的基金种类
		var KindIndex = e.index;

		Ti.API.info("clicked on table view = " + e.source);
		Ti.API.info("clicked on row index = " + e.index);

		//针对两个标题分两段处理index值
		if(KindIndex <= 2) {
			KindIndex = KindIndex + 9;
		} else {
			//2012.6.20 配置型替换为指数型，为兼容旧版本，空出配置型的id，给指数型一个新的id
			if(KindIndex == 9) {
				KindIndex = -1;
			} else {
				KindIndex = KindIndex - 3;
			}
		}

		Ti.API.info('real save index:' + KindIndex);
		Titanium.App.Properties.setString('UserFundKind', KindIndex);
		//用来区分用户是从此页面还是从基金公司列表页进入基金列表
		Titanium.App.Properties.setString('fundListDisplayType', 'fund');
		//创建基金列表窗口
		var winFundList = Titanium.UI.createWindow({
			color : 'white',
			url : 'tabFundList.js',
			title : e.rowData.title
		});

		Titanium.UI.currentTab.open(winFundList, {
			animated : true
		});

		var navActInd = Titanium.UI.createActivityIndicator();

		//创建基金列表窗口右上工具栏的等待图标
		winFundList.setRightNavButton(navActInd);

		navActInd.show();
		setTimeout(function() {
			navActInd.hide();
		}, 2000);
	} catch(e) {
		ShowInfo('数据文件没有正确下载，请检查互联网连接，或者重新下载数据')
	}
});

// 2012.6.22 #153 基金净值，头部左侧添加基金公司按钮，搜索按钮移至右侧与刷新按钮并列
//搜索按钮响应事件
function navSearchFund() {
	var w = Titanium.UI.createWindow({
		url : 'tabFundSearch.js',
		title : '基金搜索'
	});

	Titanium.UI.currentTab.open(w, {
		animated : true
	});

}

//刷新按钮响应事件
function navRefreshData() {
	//Reset LocalFundFile Date
	Titanium.App.Properties.setString('LocalFundFileDate', null);
	RefreshData();
}
//生成buttonBar
var ldir = '../../images/buttons/';
var buttonObjects = [{
	image : ldir + 'icon_search.png',
	width : 40,
	top : 5
}, {
	image : ldir + 'icon_refresh.png',
	width : 40,
	top : 5
}];
var navRight = Titanium.UI.createButtonBar({
	labels : buttonObjects,
	backgroundColor : 'faa61a'
});
navRight.addEventListener('click', function(e) {
	if(e.index == 0) {
		navSearchFund();
	} else {
		navRefreshData();
	}
});
if(Ti.Platform.name == "iPhone OS") {
	win.setRightNavButton(navRight);
}
//基金公司btn
var btnCompany = Ti.UI.createButton({
	title : '基金公司',
	width : 100,
	height : 30
});
btnCompany.addEventListener('click', function(e) {
	var w = Titanium.UI.createWindow({
		url : 'tabFundCompanyList.js',
		title : '基金公司列表'
	});
	Titanium.UI.currentTab.open(w, {
		animated : true
	});

});
win.setLeftNavButton(btnCompany);
win.add(tableviewFund);
//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
