/*

 2011.6.24. 用户自定义基金功能
 2011.6.25.
 2011.6.28. 提高速度优化
 2011.7.17. 开始使用myApp,可视控件对象化
 2011.9.25. 优化代码格式
 2011.9.27. #173 修改基金呈现方式
 2011.9.30. #174 自定义基金，不需要每次都重新刷新tableview
 2011.10.1. #183 自定义基金，编辑时候，删除按钮显示中文“删除”
 2011.10.4. #184 自定义基金，根据基金数据更新而更新
 win.addEventListener('focus'…
 function RefreshUD()
 2011.10.5. #163 在tableview左上“增加”按钮，直接可以查询基金列表
 增加btnAdd
 "增加"按钮点击后处理 btnAdd.addEventListener('click', function()
 2011.10.6. #190 自定义基金，窗口bar颜色修改为标准橙色
 2011.10.8. #196 自定义基金，调整基金显示顺序自动保存
 增加函数 function SaveUD()
 修改 btnCancel.addEventListener('click'
 修改 tableviewUD.addEventListener('delete',
 2011.10.9. #228 自定义基金，第一次使用没有基金数据时候报错
 修改 win.addEventListener('focus'…
 2011.10.9. #229 “我的基金”在调整顺序时，基金净值超过10以上时 日增幅数据会遮住净值数据
 修改 function RefreshUD()
 2011.11.26. #354 调整调用库函数方式
 2012.2.6. #500 关注基金，调整tableview生成方法
 2012.4.28. ac #40: 关注基金，tableview中label高度有问题
 2012.6.20 #147 关注基金、基金列表和基金内页，对于日涨幅数据的描述不统一

 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;
//2011.10.6. #190 自定义基金，窗口bar颜色修改为标准橙色
win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

var UD_FundCount;
var UD_FundCode = [];

var UD_NeedRefresh;

var FundCodeName, FundItem, FundCode, FundNet, p, q, FundReturn, FundName, FundCount;

//#183 自定义基金，编辑时候，删除按钮显示中文“删除”
//2011.10.1.
var tableviewUD = Titanium.UI.createTableView({
	backgroundColor : 'transparent',
	style : Titanium.UI.iPhone.TableViewStyle.GROUPED
});

var FundData = [];

//从总体数组FundData中获得指定fundcode的fund基础信息
//2011.
function GetFundDetail(lFundCode) {

	var i = 0;
	var b = true;

	while(b) {
		//控制循环
		if(i == (FundData.length)) {
			b = false;
		} else {

			if(lFundCode == FundData[i].FundCode) {
				FundCode = lFundCode;
				FundName = FundData[i].FundName;
				FundNet = FundData[i].FundNet;
				FundReturn = FundData[i].FundReturn;
				b = false;
			}
			i++;
		}
	}
}

//显示用户自定基金内容到tableview中
//2011.6.25.
//2011.9.27. #173 修改基金呈现方式
//2011.10.4. #174 自定义基金，不需要每次都重新刷新tableview
//2011.10.9. #229 “我的基金”在调整顺序时，基金净值超过10以上时 日增幅数据会遮住净值数据
//2012.2.6. #500 关注基金，调整tableview生成方法
//2012.4.28. ac #40: 关注基金，tableview中label高度有问题
function RefreshUD() {
	Ti.API.info('UD RefreshUD executing…');

	//读取当前基金文件日期
	var LocalFundFileDate = Titanium.App.Properties.getString('LocalFundFileDate');

	//获得用户自定义基金数量
	UD_FundCount = Titanium.App.Properties.getInt('UD_FundCount');
	Ti.API.info('User Define Fund Count is:' + UD_FundCount);

	//用户没有自定义过，初始化
	if((UD_FundCount == null) || (UD_FundCount == 0)) {
		tableviewUD.data = null;
		ShowInfo('没有设置过关注基金');
	} else {

		//读出用户自定义数组
		UD_FundCode = Titanium.App.Properties.getList('UD_FundCode');

		Ti.API.info('UD_FundCode:' + UD_FundCode);

		tableviewUD.data = null;
		var rowdata = [];
		for( i = 0; i < UD_FundCount; i++) {
			Ti.API.info('ud ' + UD_FundCode[i]);

			//根据用户自定义FundCode获得该基金的详细信息
			GetFundDetail(UD_FundCode[i]);

			//判断基金名称是否过长需要截断显示
			if(CnGetLen(FundName) > 16) {
				FundName = CnSubString(FundName, 15, true);
			}

			//2012.4.28. ac #40: 关注基金，tableview中label高度有问题
			//以下为创建tableview中显示的标签
			var lblFundname = wing.ui.createLabel({
				text : FundName,
				color : 'black',
				top : 8,
				left : 5,
				height : 12,
				font : {
					fontWeight : '',
					fontSize : 12
				},
				clickname : FundCode
			});

			var lblFundcode = wing.ui.createLabel({
				text : FundCode,
				color : 'black',
				bottom : 2,
				left : 5,
				height : 12,
				font : {
					fontWeight : '',
					fontSize : 12
				},
				clickname : FundCode
			});

			var lblFundnet = wing.ui.createLabel({
				text : FundNet,
				shownumber : true,
				top : 7,
				left : 110, //2011.10.9. #229 “我的基金”在调整顺序时，基金净值超过10以上时 日增幅数据会遮住净值数据
				height : 20,
				font : {
					fontWeight : 'bold',
					fontSize : 20
				},
				clickname : FundCode
			});

			var lblFunddate = wing.ui.createLabel({
				text : LocalFundFileDate,
				color : 'gray',
				bottom : 1,
				left : 114,
				height : 10,
				font : {
					fontWeight : '',
					fontSize : 10
				},
				clickname : FundCode
			});

			var lblFundreturn = wing.ui.createStockLabel({
				text : FundReturn,
				colorlabel : true,
				top : 12,
				left : 190,
				height : 20,
				font : {
					fontWeight : 'bold',
					fontSize : 20
				},
				clickname : FundCode
			});

			var row = Ti.UI.createTableViewRow();
			row.height=43;
			row.add(lblFundname);
			row.add(lblFundcode);
			row.add(lblFundnet);
			row.add(lblFunddate);
			row.add(lblFundreturn);
			row.clickname = FundCode;
			row.hasChild = true;
			row.backgroundColor = 'white';

			//显示header
			if(i == 0) {
				//2012.6.20 #147 关注基金、基金列表和基金内页，对于日涨幅数据的描述不统一
				row.header = '基金名称       净值          日涨幅';
			}

			rowdata.push(row)
		}
		tableviewUD.data = rowdata;

		//设置更新标志为false，不需要再更新
		Titanium.App.Properties.setBool('UD_NeedRefresh', false);
	}
}

//编辑按钮
var btnEdit = Titanium.UI.createButton({
	title : '编辑'
});

//编辑按钮
//2011.6.25.
btnEdit.addEventListener('click', function() {

	var winUDEdit = Titanium.UI.createWindow({
		url : 'tabUDEdit.js',
		//title : e.rowData.title
	});
	//winUDEdit.open();
	//winUDEdit.open({transition:Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});
	winUDEdit.hideTabBar();
	Titanium.UI.currentTab.open(winUDEdit, {
		animated : true
	}); win
});

//2011.6.24. copy from tabList.js
//tableview基金列表点击事件
tableviewUD.addEventListener('click', function(e) {
	//点击的基金代码
	var UserFundCode;
	UserFundCode = e.source.clickname;

	Titanium.App.Properties.setString('UserFundCode', UserFundCode);
	var winFundDetail = Titanium.UI.createWindow({
		url : 'tabFundDetail.js',
		title : e.rowData.title
	});

	Titanium.UI.currentTab.open(winFundDetail, {
		animated : true
	});
});
//2011.10.9. #228 自定义基金，第一次使用没有基金数据时候报错
//2011.10.4. #184 自定义基金，根据基金数据更新而更新
//2012.2.15. #507 关注基金，如果用户第一次运行，没有基金基础数据，不能进行操作
win.addEventListener('focus', function(e) {
	UD_NeedRefresh = Titanium.App.Properties.getBool('UD_NeedRefresh');
	if(UD_NeedRefresh) {
		//读取基金数据
		FundData = Titanium.App.Properties.getList('FundData');

		//如果基金数据是空
		if(FundData == null) {
			ShowInfo('没有基金数据，请到“基金净值”功能中下载最新基金净值');
			win.setRightNavButton(null);
			Ti.API.info('fund data not found');
		} else {
			win.setRightNavButton(btnEdit);
			RefreshUD();
		}
	}
});

win.add(tableviewUD);

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
