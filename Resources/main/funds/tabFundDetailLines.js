/*
 * 2012.6.6. ac #107 基金内页，走势图页面图表显示
 * 2012.6.8. ac #119 基金内页，走势图详情页图片可拖动
 * 2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
 *
 */
Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);
//2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
var fund_code = Titanium.App.Properties.getString('fund_code_detail', '');
var fund_name = Titanium.App.Properties.getString('fund_name_detail', '');
if (getStrLength(fund_name) > 20) {
	fund_name = CnSubString(fund_name, 19, true);
}
labelTitle = Ti.UI.createLabel({
	text : fund_name + '\r' + fund_code,
	height : 43,
	color : 'white',
	textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
	width : '70%',
	font : {
		fontSize : 14,
		fontWeight : 'bold'
	},
});
win.setTitleControl(labelTitle);
CheckInternet();
var line_type = Titanium.App.Properties.getString('line_type', '');
//webView高度
var height_web = 270;
//webView宽度
var width_web = 270;
//webView内DIV高度，留白便于美观
var height_div = height_web - 20;
var view = Ti.UI.createView({
	height : height_web + 10,
	top : 10,
	width : '95%',
	backgroundColor : 'white',
	borderRadius : 5,
});
var webView = Ti.UI.createWebView({
	backgroundColor : 'transparent',
	height : height_web,
	width : '100%',
	top : 0,
	left : -15,
	borderRadius : 5,
	url : 'http://www.ttyfund.com/api/mobile/fundLine.php?fund_code=' + fund_code + '&line_type=' + line_type + '&height=' + height_div + '&width=' + width_web
});
Ti.API.info('webView url: ' + webView.url);
//2012.6.8. ac #119 基金内页，走势图详情页图片可拖动
webView.addEventListener('load', function(evt) {
	this.evalJS('document.addEventListener("touchmove", function(e){ e.preventDefault(); });')
});
view.add(webView);
win.add(view);
//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
