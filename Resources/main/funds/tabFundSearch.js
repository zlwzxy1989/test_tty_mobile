/*
 File: tabFundSearch.js
 Function:代码搜索窗口

 2011.6.27. created
 2011.10.6. #194 修改win bar背景色
 2011.11.26. #354 调整调用库函数方式
 2012.2.12. #503 关注基金功能相关调整
 2012.3.13. #555 关注基金，搜索结果显示异常
 2012.6.26. ac #176 增加高度定义，避免出现大量警告

 */
Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');
Ti.include('../../tty_tools/consts.js');

var win = Titanium.UI.currentWindow;
//#194 修改win bar背景色
win.barColor = 'faa61a';
win.backgroundColor = 'white';
win.backgroundImage = GetBackgroundFile(2);

var my = {};
my.ud_result = false;

var navActInd = Titanium.UI.createActivityIndicator({
	font : {
		fontFamily : 'Helvetica Neue',
		fontSize : 12,
		fontWeight : 'bold'
	},
	color:'white',
	message : '请稍等'
});
if (Titanium.Platform.name == 'iPhone OS') {
	win.setRightNavButton(navActInd);
}

navActInd.show();

setTimeout(function() {
	navActInd.hide();
}, 2000);

var FundCode, FundName, FundNet, FundReturn;

var search = Titanium.UI.createSearchBar({
	showCancel : false,
	hintText : '输入基金代码或者名称'
});
search.addEventListener('change', function(e) {
	// e.source.value;// search string as user types
});
search.addEventListener('return', function(e) {
	e.source.blur();
});
search.addEventListener('cancel', function(e) {
	e.source.blur();
});
var FundData = [];
FundData = Titanium.App.Properties.getList('FundData');

//处理自定义图片部分
//2012.2.11. #503 关注基金功能相关调整
//2012.6.26. ac #176 增加高度定义，避免出现大量警告
function DoUDData() {
	var UD = Titanium.App.Properties.getList('UD_FundCode');
	var i, j;

	Ti.API.info('ud:' + UD);

	for ( i = 0; i < FundData.length; i++) {
		FundData[i].leftImage = '../../images/system/btn_remove_fund.png';
		FundData[i].ud = false;
		FundData[i].hasChild = false;
		FundData[i].backgroundColor = 'white';
		FundData[i].height=0;

		if (UD != null) {
			for ( j = 0; j < UD.length; j++) {
				if (UD[j] == FundData[i].FundCode) {
					FundData[i].leftImage = '../../images/system/btn_add_fund.png';
					FundData[i].ud = true;
				}
			}
		}
	}
}

DoUDData();

//增加选中的基金到自定义基金
//2012.2.12. #503 关注基金功能相关调整
function AddFund2UD(UserFundCode) {

	my.ud_result = false;

	var UD_FundCode = Titanium.App.Properties.getList('UD_FundCode');
	var UD_FundCount = Titanium.App.Properties.getInt('UD_FundCount');
	Ti.API.info('UD_FundCode:' + UD_FundCode);
	Ti.API.info('User Define Fund Count is:' + UD_FundCount);

	if (UD_FundCount === ud_max) {
		ShowInfo('您的关注基金数量已经到了' + ud_max + '个的限制');
	} else {

		//用户没有自定义过，初始化
		if ((UD_FundCount === null) || (UD_FundCount === 0)) {
			UD_FundCount = 0;
			UD_FundCode = [];
		};

		if (UD_FundCode.inArray(UserFundCode)) {

		} {

			Ti.API.info('ud want:' + UserFundCode);
			Ti.API.info('UD_FundCount:' + UD_FundCount);
			UD_FundCode[UD_FundCount] = UserFundCode;

			Ti.API.info('ud FundCode:' + UD_FundCode[UD_FundCount]);
			UD_FundCount++;
			Titanium.App.Properties.setInt('UD_FundCount', UD_FundCount);
			Ti.API.info('User Define Fund Count now is:' + UD_FundCount);

			Titanium.App.Properties.setList('UD_FundCode', UD_FundCode);
			Ti.API.info('UD:' + UD_FundCode);

			ShowInfo('已经添加到关注基金');

			my.ud_result = true;
			my.ud_act = 'add';

			//设置UD强制刷新
			Titanium.App.Properties.setBool('UD_NeedRefresh', true);
		}
	}
}

//删除选中的自定义基金
//2012.2.12. #503 关注基金功能相关调整
function DelFund2UD(UserFundCode) {

	my.ud_result = true;

	var UD_FundCode = Titanium.App.Properties.getList('UD_FundCode');
	var UD_FundCount = Titanium.App.Properties.getInt('UD_FundCount');
	Ti.API.info('UD_FundCode:' + UD_FundCode);
	Ti.API.info('User Define Fund Count is:' + UD_FundCount);

	//数组中删除
	UD_FundCode.deleteByValue(UserFundCode);

	//计数器删除
	UD_FundCount = UD_FundCount - 1;

	//数组和计数器存盘
	Titanium.App.Properties.setInt('UD_FundCount', UD_FundCount);
	Ti.API.info('User Define Fund Count now is:' + UD_FundCount);

	Titanium.App.Properties.setList('UD_FundCode', UD_FundCode);

	ShowInfo('已经从关注基金中删除');

	my.ud_act = 'remove';

	//设置UD强制刷新
	Titanium.App.Properties.setBool('UD_NeedRefresh', true);

}

// tableview object
//2012.3.13. #555 关注基金，搜索结果显示异常
var tableviewSearch = Titanium.UI.createTableView({
	data : FundData,
	style : Titanium.UI.iPhone.TableViewStyle.GROUPED,
	search : search,
	rowHeight : 43,
	filterAttribute : 'filter',
	backgroundColor : 'transparent'
});

//2011.6.24. copy from tabList.js
//tableview基金列表点击事件
//#503 关注基金功能相关调整
tableviewSearch.addEventListener('click', function(e) {

	win.touchEnabled = false;

	var UD_FundCode = Titanium.App.Properties.getList('UD_FundCode');
	var UD_FundCount = Titanium.App.Properties.getInt('UD_FundCount');

	//用户没有自定义过，初始化
	if ((UD_FundCount === null) || (UD_FundCount === 0)) {
		UD_FundCount = 0;
		UD_FundCode = [];
	};

	//是否已经在关注基金中
	if (UD_FundCode.inArray(FundData[e.index].FundCode)) {
		Ti.API.info('del fundcode:' + FundData[e.index].FundCode);
		DelFund2UD(FundData[e.index].FundCode);
	} else {
		Ti.API.info('add fundcode:' + FundData[e.index].FundCode);
		AddFund2UD(FundData[e.index].FundCode);
	}

	tableviewSearch.scrollToIndex(e.index);

	if (my.ud_result) {

		//用户点击row后，更新该行row的数据，调整选中图片
		var rowdata = {
			title : FundData[e.index].title,
			filter : FundData[e.index].filter,
			clickname : FundData[e.index].clickname,
			FundCode : FundData[e.index].FundCode,
			//FundName : FundData[e.index].FundName,
			//FundNet : FundData[e.index].FundNet,
			//FundReturn : FundData[e.index].FundReturn,
			//FundKind : FundData[e.index].FundKind,
			backgroundColor : 'white',
		}

		Ti.API.info('ud_act:' + my.ud_act);
		if (my.ud_act == 'add') {
			rowdata.leftImage = '../../images/system/btn_add_fund.png'
		}
		if (my.ud_act == 'remove') {
			rowdata.leftImage = '../../images/system/btn_remove_fund.png'
		}
		tableviewSearch.updateRow(e.index, rowdata);
	}

	win.touchEnabled = true;

});

win.add(tableviewSearch);

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
