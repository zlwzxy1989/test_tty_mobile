/**
 * 历史净值、拆分、分红页面 created by zxy
 * 2012.6.6. ac #106 基金内页，基金历史净值页面数据读取、显示
 * 2012.6.6. ac #108 基金内页，基金分红页面数据读取、显示
 * 2012.6.6. ac #109 基金内页，基金拆分页面数据读取、显示
 * 2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
 * 2012.6.18. ac #140 基金内页，二级菜单详情页如“基金详情”详情页，首行添加“基金详情”
 * 2012.6.18. ac #141 基金内页，基金分红和基金拆分内页列表时间按从新到旧排序
 * 2012.6.18. ac #142 基金内页，有二级目录的标题与新闻列表格式统一
 * 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
 */
Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);
//2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
var fund_code = Titanium.App.Properties.getString('fund_code_detail', '');
var fund_name = Titanium.App.Properties.getString('fund_name_detail', '');
var my = {};
//tableView数据数组
my.data = [];
my.header = {};
if(getStrLength(fund_name) > 20) {
	fund_name = CnSubString(fund_name, 19, true);
}
labelTitle = Ti.UI.createLabel({
	text : fund_name + '\r' + fund_code,
	height : 43,
	color : 'white',
	textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
	width : '70%',
	font : {
		fontSize : 14,
		fontWeight : 'bold'
	},
});
win.setTitleControl(labelTitle);
CheckInternet();
var line_type = Titanium.App.Properties.getString('line_type', '');
// 以下参数先写为默认，然后根据line_type重新设置
//调用的接口方法
var act = 'GetFundTrend';
//是否是净值类数据。净制类数据第三列为涨跌，需要计算，数字将根据涨跌情况显示不同的颜色
var isNav = true;
//返回数据的键名。便于合并返回数据结构相似的数据处理部分的代码
var response_key;
switch (line_type) {
	//净值
	case 'nav':
		response_key = 'unitvalue';
		my.data.push('日期');
		my.data.push('净值');
		my.data.push('日涨幅');
		my.header={"index":[0],"text":["基金历史净值列表"]};
		ui_filename="tabFundDetailHistoryNav.ui";		
		break;
	/* 可能显示其他类型的历史数据，预留分类
	case 'accm_nav':
	response_key = 'accuvalue';
	break;
	case 'yield':
	response_key = 'day7return';
	break;
	case 'gain':
	response_key = 'milreturn';
	break;
	*/
	//拆分
	case 'chaifen':
		act = 'GetFundSplit';
		response_key = 'split';
		my.data.push('拆分日期');
		my.data.push('拆分比例');
		my.data.push('拆分类型');
		my.header={"index":[0],"text":["基金拆分"]};				
		ui_filename="tabFundDetailHistoryChaifen.ui";				
		isNav = false;
		break;
	//分红
	case 'fenhong':
		act = 'GetFundDividend';
		response_key = 'dividend';
		my.data.push('除息日');
		my.data.push('权益登记日');
		my.data.push('每份分红');
		my.header={"index":[0],"text":["基金分红"]};	
		ui_filename="tabFundDetailHistoryFenhong.ui";				
		isNav = false;
		break;
}
my.getFundHistory = Ti.Network.createHTTPClient();
var arr_getFundHistory = {};
var json_getFundHistory = {};
arr_getFundHistory.key = 'TTYFUND-CHINAPNR';
arr_getFundHistory.act = act;
arr_getFundHistory.symbol = fund_code;
arr_getFundHistory.url = 'http://www.ttyfund.com/api/services/FundService.php';
url_getFundHistory = getApiUrl(arr_getFundHistory);
Ti.API.info('url_getFundHistory:' + url_getFundHistory);
my.getFundHistory.open("GET", url_getFundHistory);
my.getFundHistory.onload = function() {
	try {
		Ti.API.info(my.getFundHistory.responseText);
		json_getFundHistory = JSON.parse(my.getFundHistory.responseText);
		// 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
		if(isNav == true) {
			//净值类
			if(json_getFundHistory.return_code == 1) {
				data_num = json_getFundHistory[response_key].length;
				var data_last= data_num - 11;
				if (data_last<0)
				{
					data_last=0;
				}			
				for(var i = data_num - 1; i > data_last; i--) {
					//接口返回的数据从旧到新，需要从尾部开始取
					var date, nav_today, nav_yesterday, day_return;
					//净值日期
					date = dateFormat(json_getFundHistory[response_key][i]['date']);
					//净值日期当天的净值
					nav_today = parseFloat(json_getFundHistory[response_key][i]['value']).toFixed(4);
					//净值日期前一交易日的净值
					nav_yesterday = parseFloat(json_getFundHistory[response_key][i-1]['value']).toFixed(4);
					//接口不返回日涨幅，需计算
					day_return = ((nav_today - nav_yesterday) / nav_yesterday) * 100;
					day_return = day_return.toFixed(2) + '%';				
					my.data.push(date);
					my.data.push(nav_today);
					my.data.push(day_return);				
				}
				showTableView();
			}
		} else {
			if(json_getFundHistory.return_code == 'no ' + response_key + ' info') {
				ShowInfo('暂无' + my.header.text[0] + '信息');
			} else if(json_getFundHistory.return_code == 1) {
				data = json_getFundHistory[response_key];
				data_num = data.length;
				Ti.API.info('data: ' + data);
				//2012.6.18. ac #141 基金内页，基金分红和基金拆分内页列表时间按从新到旧排序
				for(var i = 0; i <= data_num - 1; i++) {
					var dataType = null;
					if(line_type == 'chaifen') {
						//拆分
						//拆分日期
						text1 = dateFormat(data[i]['effective_date']);
						//拆分比例
						text2 = parseFloat(data[i]['ratio']).toFixed(4);
						//拆分类型
						text3 = data[i]['type'];
					} else if(line_type == 'fenhong') {
						//分红
						//除息日
						text1 = dateFormat(data[i]['excluding_date']);
						//权益登记日
						text2 = dateFormat(data[i]['record_date']);
						// 2012.6.18. ac #142 基金内页，有二级目录的标题与新闻列表格式统一
						//每份分红
						text3 = parseFloat(data[i]['distribution']).toFixed(4);
					}
					my.data.push(text1);
					my.data.push(text2);
					my.data.push(text3);				
				}
				showTableView();
			}
		}
	} catch(E) {
		Ti.API.info(E);
	}

}
my.getFundHistory.send();
/*
 * 显示tableView
 * 传入：无
 * 传出：无
 * */
function showTableView() {
	tableview = wing.ui.createTableView({
		backgroundColor : 'transparent',
		type : tableviewLabels,
		value : my.data,
		ui_filename : ui_filename,
		header:my.header,
		needTitle:true
	});	
	win.add(tableview);
}
//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
