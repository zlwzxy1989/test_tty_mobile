/*

 wing_ui.js UI

 2011.7.17. create
 2011.7.18. change to oo
 2011.9.12. edit
 2011.10.14. #238 myui，add function formatnumber
 ----
 2011.10.30. add wing.ui.window object
 add wing.ui.TableView object
 add wing.ui.RowSwitch object

 2011.10.31. #9 add tableview text object

 2011.11.1. #284 create tablerow object
 #285 create rowPicker object
 2011.11.3. edit rowPicker to support date picker
 #290 rowText support keyboard
 2011.11.11. #317 ui object add id，itemType
 2011.11.25. #345 ui row object
 #347: ui，wing.ui.createRowText add change event
 2011.11.28. #358 ui object button
 2011.12.5.  #362 add button glass effect
 2011.12.16. optimize code
 2011.12.24. #433 add 5 color to glass button
 2012.1.1. move from tools #443 修改参数调用为_args模式
 #442 支持tableview点击显示web url
 2012.1.3. #445 读取背景图案设置
 2012.1.7. #431 输入时防止其他控件输入
 2012.3.14. #556 界面,web界面支持在底部放置一个半透明按钮用来关闭web
 2012.3.23. #567 系统优化，显示基金净值的算法优化
 2012.4.20. 设置默认tableview高度
 2012.4.28. ac #39 label 默认height修改为参数值，避免警告
 2012.5.7. #53 #54 根据sdk 2.0.1 修改界面小问题
 2012.5.19. #78 如果是数字键盘类型，过滤用户输入的空格
 2012.6.25. #175 createTableViewLabels() 避免label的width警告
 2012.7.5. #184 天天盈登录，登录后点击任意按钮功能正常但loading样式不消失
 2012.7.31 #189 库函数，wing.ui.createTableView解析ui文件部分改为解析json格式内容 by zxy
 2012.7.31 #190 库函数，createTableViewSimple生成tableView部分修改 by zxy
 2012.7.31 #191 库函数，createTableViewLabels生成tableView部分修改 by zxy
 2012.7.31 #192 库函数，createTableViewLabels增加设置header功能 by zxy
 2012.7.31 #193 库函数，createTableViewLabels增加设置style功能 by zxy
 2012.7.31 #194 库函数，createTableViewLabels增加在已有tableView上增加行的功能 by zxy
 2012.8.1  #195 库函数，增加createTableViewComplex方法 by zxy
 2012.8.1 #203 wing.ui.createLabel做android兼容性修改 by zxy
 2012.8.2 #204 库函数，createTableViewLabels增加传入参数needTitle by zxy

 */

Ti.include('../tools/tools.js');
//
rowSwitch = 10001;
rowText = 10002;
rowPicker = 10003;
rowLabels = 10004;
//
keyboardDefault = 10101;
keyboardNumbers = 10102;
keyboardNumberPad = 10103;
//
pickerDate = 10201;
pickerSingle = 10202;
//
buttonCommon = 10301;
buttonGlass = 10302;
buttonGlassDark = 10351;
buttonGlassGreen = 10352;
buttonGlassGray = 10353;
buttonGlassRed = 10354;
buttonGlassYellow = 10355;
//
textAlignLeft = 10400;
textAlignRight = 10401;
textAlignCenter = 10402;
//
tableviewSimple = 10500;
tableviewLabels = 10501;
tableviewControls = 10502;
tableviewComplex = 10503;

var wing = {};

//create a ui namespace
wing.ui = {};

var viewShadow = Titanium.UI.createView({
	opacity : 0.9,
	color : 'black'
});

//2011.12.23. label object
//2012.4.28. ac #39 label 默认height修改为参数值，避免警告
//2012.7.11. android 兼容性修改
//2012.8.1 #203 wing.ui.createLabel做android兼容性修改
wing.ui.createLabel = function(/*Object*/_args) {

	var v = Ti.UI.createLabel({
		text : _args.text,
		color : _args.color || 'black',
		backgroundColor : _args.backgroundColor || 'white',
		textAlign : _args.text_align || 'left',
		width : _args.width || 'auto',
		height : _args.height || 'auto',
		clickname : _args.clickname,
		font : _args.font || {
			fontWeight : 'bold',
			fontSize : 14,
			fontFamily : 'Arial'
		},
		zIndex : _args.zIndex || 1
	});
	if(Ti.Platform.name == "iPhone OS") {
		if(_args.top != undefined) {
			v.top = _args.top;
		}
		if(_args.bottom != undefined) {
			v.bottom = _args.bottom;
		}
		if(_args.left != undefined) {
			v.left = _args.left;
		}
		if(_args.right != undefined) {
			v.right = _args.right;
		}
	} else if(Ti.Platform.name == "android") {
		v.top = _args.top || 'auto';
		v.bottom = _args.bottom || 'auto';
		v.left = _args.left + _args.offset || 'auto';
		v.right = _args.right || 'auto';
	}

	return v;
};

//ui label object
//property colorlabel
//2011.9.27. add _args.color;
//2011.11.22. edit name to createStockLabel
//2011.12.23. edit for createLabel
//2012.3.23. #567 系统优化，显示基金净值的算法优化
//2012.7.11. android 兼容性修改
wing.ui.createStockLabel = function(/*Object*/_args) {

	var ltext, lback = 'white', lcolor = 'black', lgreen;

	if(Ti.Platform.name == "iPhone OS") {
		lgreen = '669966'
	} else {
		lgreen = 'green'
	}

	if(_args.shownumber == true) {
		//ltext = formatNumber(_args.text, '0.0000')
		ltext = Number(_args.text).toFixed(4);
	} else {
		ltext = _args.text
	}

	if(_args.colorlabel == true) {
		lcolor = 'white';

		if(_args.text.substring(0, 1) == '-') {
			lcolor = lgreen
		} else if(_args.text == '0.00%') {
			lcolor = 'gray'
		} else {
			lcolor = 'red'
		}
	}

	_args.color = lcolor;
	_args.backgroundColor = _args.setbgColor || lback;

	var v = wing.ui.createLabel(_args);

	return v;
};
// #433 add 5 color to glass button
// #362, add button glass effect
// #358, create wing ui button object
// create 2011.11.28.
wing.ui.createButton = function(/*Object*/_args) {

	var v = Ti.UI.createButton({
		title : _args.title,
		height : _args.height || 57,
		width : _args.width || 301,

		font : _args.font || {
			fontSize : 16,
			fontFamily : 'Arial'
		},

		top : _args.top,
		bottom : _args.bottom,
		left : _args.left,
		right : _args.right
	});

	switch (_args.type) {
		case buttonCommon:
			v.borderRadius = _args.borderRadius || 10;
			v.borderWidth = _args.borderWidth || 2;
			v.borderColor = _args.borderColor || 'gray';
			v.backgroundColor = _args.backgroundColor || 'white';
			v.backgroundImage = _args.backgroundImage || 'NONE';
			v.color = _args.color || 'black';
			v.selectedColor = _args.selectedColor || 'blue';
			v.setBackgroundSelectedImage = _args.setBackgroundSelectedImage || '../tools/ui/button/BUTT_grn_on.png'
			break;
		case buttonGlass:
			v.color = _args.color || 'white';
			v.selectedColor = _args.selectedColor || 'black';
			break;
	};

	var ldir = '../tools/ui/button/';
	if(_args.dir_level == 2) {
		ldir = '../' + ldir;
	}

	switch (_args.buttonGlassColor) {
		case buttonGlassDark:
			v.backgroundImage = ldir + 'BUTT_drk_off.png';
			v.backgroundSelectedImage = ldir + 'BUTT_drk_on.png';
			break;
		case buttonGlassGreen:
			v.backgroundImage = ldir + 'BUTT_grn_off.png';
			v.backgroundSelectedImage = ldir + 'BUTT_grn_on.png';
			break;
		case buttonGlassGray:
			v.backgroundImage = ldir + 'BUTT_gry_off.png';
			v.backgroundSelectedImage = ldir + 'BUTT_gry_on.png';
			break;
		case buttonGlassRed:
			v.backgroundImage = ldir + 'BUTT_red_off.png';
			v.backgroundSelectedImage = ldir + 'BUTT_red_on.png';
			break;
		case buttonGlassYellow:
			v.backgroundImage = ldir + 'BUTT_yel_off.png';
			v.backgroundSelectedImage = ldir + 'BUTT_yel_on.png';
			break;
	}

	return v;
};
//wing window object
//2011.10.30. create
wing.ui.createWindow = function(/*Object*/_args) {

	var v = Ti.UI.createWindow({
		url : _args.url,
		title : _args.title,
		color : _args.color,
		barColor : _args.barColor || 'white',
		backgroundImage : '../images/bg/' + _args.backgroundImage
	});

	return v;
};
//2011.6.22.
//2011.10.8. #223 调用浏览器可以设置标题栏颜色
//2011.10.31. #280 修改浏览器窗口标题栏可以不显示
//2011.11.8. #304 修改检查internet链接，不显示出错信息
//2011.11.8. #309 增加ForceLoad参数，设置在未联网状态强制读入web
//2012.1.1. move from tools #443 修改参数调用为_args模式
//2012.3.14. #556 界面,web界面支持在底部放置一个半透明按钮用来关闭web
wing.ui.createWeb = function(/*Object*/_args) {
	//function OpenWeb(Url, Title, CloseNav, BarColor, HideNav, ForceLoad) {

	if((CheckInternet(false) == true) || (_args.ForceLoad == true)) {
		var w;
		w = Ti.UI.createWindow();

		if(_args.HideNav === true) {
			w = Ti.UI.createWindow();
			w.hideNavBar();
		} else {
			w = Ti.UI.createWindow();
		}

		Ti.API.info('browser url:' + _args.Url);
		w.title = _args.Title;
		w.barColor = _args.BarColor;

		var webMain = Ti.UI.createWebView({
			url : _args.Url
		});
		w.add(webMain);

		webMain.anchorPoint = (0.5, 0.5);

		var actInd = Titanium.UI.createActivityIndicator({
			height : 50,
			width : 10,
			style : Titanium.UI.iPhone.ActivityIndicatorStyle.DARK
		});
		w.add(actInd);

		Titanium.UI.currentTab.open(w, {
			animated : true
		});

		webMain.addEventListener('beforeload', function(e) {
			// 2012.7.5. #184 天天盈登录，登录后点击任意按钮功能正常但loading样式不消失
			Ti.API.info('begin to load ' + e.url);
			if(e.url.indexOf("#") == -1)
				actInd.show();
		});
		webMain.addEventListener('load', function(e) {
			Ti.API.info('load ok ' + e.url);
			actInd.hide();
		});
		//
		// NAVBAR
		//
		var ldir = '../images/buttons/';
		if(_args.dir_level == 2) {
			ldir = '../' + ldir;
		}
		var buttonObjects = [{
			image : ldir + 'arrow_back.png',
			width : 30,
			top : 5
		}, {
			image : ldir + 'arrow_forward.png',
			width : 30,
			top : 5
		}];

		var bbNav = Titanium.UI.createButtonBar({
			labels : buttonObjects,
			backgroundColor : _args.BarColor
		});

		if(Ti.Platform.name == "iPhone OS") {
			if(_args.CloseNav == true) {
			} else {
				w.setRightNavButton(bbNav);
			}
		}

		bbNav.addEventListener('click', function(e) {
			if(e.index == 0) {
				if(webMain.canGoBack() == true) {
					webMain.goBack();
				} else {
					w.close();
				}
			} else {
				if(webMain.canGoForward() == true) {
					webMain.goForward();
				}
			}
		})
		if(_args.ShowClose) {
			var btnClose = Titanium.UI.createButton({
				title : '关闭',
				height : 40,
				width : 80,
				font : {
					fontSize : 16,
					fontFamily : 'Arial'
				},
				borderRadius : 10,
				borderWidth : 2,
				borderColor : 'gray',
				backgroundColor : 'white',
				backgroundImage : 'NONE',
				selectedColor : 'blue',
				bottom : 5,
				right : 20,
				color : 'black',
				opacity : 0.5
			});

			btnClose.addEventListener('click', function(e) {
				w.close();
				w = null;
			})

			w.add(btnClose)

		}
	}

};
//wing tableview object
//2011.10.30. create
//2011.12.25.
//2012.1.1. #442 支持tableview点击显示web url
//2012.3.8. #536 支持多重路径
//2012.4.20. 设置默认tableview高度
//2012.6.25. 优化createTableViewLabels() 避免label的width警告
//2012.7.11. android 兼容性修改
//2012.7.31 #189 库函数，wing.ui.createTableView解析ui文件部分改为解析json格式内容 by zxy
//2012.7.31 #190 库函数，createTableViewSimple生成tableView部分修改 by zxy
//2012.7.31 #191 库函数，createTableViewLabels生成tableView部分修改 by zxy
//2012.7.31 #192 库函数，createTableViewLabels增加设置header功能 by zxy
//2012.7.31 #193 库函数，createTableViewLabels增加设置style功能 by zxy
//2012.7.31 #194 库函数，createTableViewLabels增加在已有tableView上增加行的功能 by zxy
//2012.8.1  #195 库函数，增加createTableViewComplex方法 by zxy
//2012.8.2 #204 库函数，createTableViewLabels增加传入参数needTitle by zxy
wing.ui.createTableView = function(/*Object*/_args) {

	var f;
	var contents;
	var rowdata = [];

	if(Ti.Platform.name == "iPhone OS") {
		f = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory + '/ui', _args.ui_filename);
		contents = f.read.blob;

	} else if(Ti.Platform.name == "android") {
		f = Titanium.Filesystem.getFile(Ti.Filesystem.resourcesDirectory + 'ui', _args.ui_filename);
		contents = f.read();
		Ti.API.info('f=' + Ti.Filesystem.resourcesDirectory + 'ui');
	}

	Ti.API.info('file = ' + f);
	Ti.API.info('f = ' + f.exists());
	//去除注释和换行 by zxy
	contents = contents.text.replace(/\s*/img, '');
	contents = contents.replace(/\/\*[^\0]*?\*\//img, '');
	Ti.API.info("ui file: " + contents);
	var lcsv = [];
	lcsv = JSON.parse(contents);
	lcsv = lcsv.ui;
	lRowCount = lcsv.length;
	function createTableViewSimple() {

		for(var i = 0; i <= lRowCount - 1; i++) {
			var row = createUiInTableView(lcsv[i]);
			rowdata.push(row);
		}
		var style = getTableViewStyle(_args.style);
		var tableview = Titanium.UI.createTableView({
			data : rowdata,
			backgroundColor : 'transparent',
			style : style
		});
		//#442 支持web url
		if(_args.web === true) {
			tableview.addEventListener('click', function(e) {
				var li;
				li = e.index;
				if(lcsv[li].hasChild) {
					var web = wing.ui.createWeb({
						Url : lcsv[i].url,
						Title : lcsv[i].title,
						CloseNav : false,
						BarColor : 'faa61a'
					})
				}
			});
		}
		return tableview;
	}

	//row with multi-labels
	function createTableViewLabels() {
		//outer for values
		var data_num = _args.value.length - 1;
		var text_tmp;
		i = 0;
		//以是否循环完数据数组为判定条件
		while(i < data_num) {
			for(var j = 0; j <= lRowCount - 1; j++) {
				text_tmp = readTextFromData(lcsv[j][0]);
				var row = createUiInTableView(lcsv[j][0], text_tmp);
				//循环其他行，逐个添加进row
				for(var k = 1; k <= lcsv[j].length - 1; k++) {
					text_tmp = readTextFromData(lcsv[j][k]);
					var label = createUiInTableView(lcsv[j][k], text_tmp);
					row.add(label);
				}
				rowdata.push(row);
			}
		}
		//未传入tableView，创建新的tableView
		if(_args.tableView == undefined) {
			//处理header,生成tableView后header无法修改
			if(_args.header != undefined) {
				for(var i_header = 0; i_header <= _args.header.index.length - 1; i_header++) {
					rowdata[_args.header.index[i_header]].header = _args.header.text[i_header];
				}
			}
			var style = getTableViewStyle(_args.style);
			var tableView = Titanium.UI.createTableView({
				data : rowdata,
				backgroundColor : _args.backgroundColor || 'transparent',
				style : style,
				top : _args.top || 0,
				left : _args.left || 0,
				width : _args.width || '100%'
			});
			if(_args.needTitle) {
				//修改表头字体
				var section, labels;
				section = tableView.data[0];
				labels = section.rows[0].getChildren();
				for(var i_labels = 0; i_labels <= labels.length - 1; i_labels++) {
					labels[i_labels].color = "black";
					labels[i_labels].height = 43;
					labels[i_labels].font.fontSize = 14;
					labels[i_labels].font.fontWeight = 'bold';
				}
			}
		}
		//有传入tableView，在tableView后加上新行
		else {
			tableView = _args.tableView;
			tableView.appendRow(rowdata);
		}
		return tableView;
	}

	//复杂tableView，所有UI参数从ui文件读取 by zxy
	function createTableViewComplex() {
		var data_num = _args.value.length - 1;
		var text_tmp;
		i = 0;
		//以是否循环完所有UI为判定条件
		for( j = 0; j <= lRowCount - 1; j++) {
			text_tmp = readTextFromData(lcsv[j][0]);
			var row = createUiInTableView(lcsv[j][0],text_tmp);
			//循环其他行，逐个添加进row
			for( k = 1; k <= lcsv[j].length - 1; k++) {
				text_tmp = readTextFromData(lcsv[j][k]);
				var ui_tableView = createUiInTableView(lcsv[j][k], text_tmp);
				row.add(ui_tableView);
			}
			rowdata.push(row);
		}
		//未传入tableView，创建新的tableView
		if(_args.tableView == undefined) {
			var style = getTableViewStyle(_args.style);
			var tableView = Titanium.UI.createTableView({
				data : rowdata,
				backgroundColor : _args.backgroundColor || 'transparent',
				style : style,
				top : _args.top || 0,
				left : _args.left || 0,
				width : _args.width || '100%'
			});
		} else {
			tableView = _args.tableView;
			tableView.appendRow(rowdata);
		}
		return tableView;
	}

	switch (_args.type) {
		case tableviewSimple:
			v = createTableViewSimple(_args);
			break;
		case tableviewLabels:
			v = createTableViewLabels(_args);
			break;
		case tableviewComplex:
			v = createTableViewComplex(_args);
			break;
	};
	return v;
	//根据不同的UI，根据ui文件的参数将填入UI的字符串进行转换 by zxy
	//传入：ui配置参数数组
	//返回：转换后的ui配置参数数组
	function readTextFromData(_args_ui) {
		var str;
		if(_args_ui.type == undefined || _args_ui.type == 'label') {
			str = getText(_args_ui.text);
		} else if(_args_ui.type == 'textArea') {
			str = getText(_args_ui.value);
		}
		return str;
	}

	//生成填入UI中字符串的内容，若ui文件里含有!VARIBLE字样则读入传入的数据 by zxy
	//传入：从ui中读入的字符串
	//返回：转换后的字符串
	function getText(ltext) {
		if(ltext == undefined) {
			ltext = '';
		} else if(/!VARIBLE/mg.test(ltext) == true) {
			_args.value[i] = _args.value[i] == undefined ? '' : _args.value[i];
			ltext = ltext.replace(/!VARIBLE/mg, _args.value[i]);
			i++;
		}
		return ltext;
	}

};
//wing tableview row object
//2011.11.25. create #345
wing.ui.createRow = function(/*Object*/_args) {

	var row = Ti.UI.createTableViewRow({
		title : _args.title,
		backgroundColor : _args.backgroundColor || 'white',
		height : _args.height || 44,
		db : _args.db || false,
		field : _args.field,
		id : _args.id,
		itemType : 'ROW'
	});
	return row;
};
//2011.11.11. #317 add property: id，itemType
//2011.10.30. create
//wing tableview switch object
wing.ui.createRowSwitch = function(/*Object*/_args) {

	var sw = Ti.UI.createSwitch({
		right : 10,
		value : _args.switch_value || false,
		id : _args.id,
		item_type : 'SWITCH'
	});

	var v = wing.ui.createRow(_args);

	//switch Change Event
	sw.addEventListener('change', function(e) {
		Titanium.App.Properties.setString('SwitchStatus', e.value);
		v.fireEvent('RowChange');
	});

	v.add(sw);

	return v;
};
//#421
//2011.12.23. tableview row with labels
wing.ui.createRowLabels = function(/*Object*/_args) {

	_args.text = _args.label1text;
	_args.left = _args.label1left || 35;
	_args.top = _args.label1top || 14;
	_args.color = _args.label1color;

	var label1 = wing.ui.createLabel(_args);

	_args.text = _args.label2text;
	_args.left = _args.label2left || 35;
	_args.top = _args.label2top || 14;
	_args.color = _args.label2color;
	var label2 = wing.ui.createLabel(_args);

	var v = wing.ui.createRow(_args);

	v.add(label1);
	v.add(label2);

	return v;
};
//2011.10.31. #9 create
//2011.11.3. #290 rowText object，support the number keyboard
//2011.11.11. #317 add property: id，itemType
//2011.11.25. #355 support row text use number pad keyboard
//2011.11.25. #347 ui，wing.ui.createRowText add change event
//2012.1.7. 设置焦点处理
//2012.5.19. #78 如果是数字键盘类型，过滤用户输入的空格
//wing tableview text object
wing.ui.createRowText = function(/*Object*/_args) {

	var tx = Ti.UI.createTextField({
		height : _args.txt_height || 30,
		top : _args.text_top || 6,
		right : _args.text_right || 10,
		width : _args.text_width || 180,
		enableReturnKey : true,
		borderStyle : Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		hintText : _args.value,
		editable : _args.editable || true,
		textAlign : _args.textAlign || 'left',
		id : _args.id,
		itemType : 'TEXTFIELD'
	});

	if(_args.editable == false) {
		tx.enabled = false
	};

	Ti.API.info('keyboard:' + _args.keyboard);

	//keyboard
	switch (_args.keyboard) {
		case keyboardDefault:
			tx.keyboardType = Titanium.UI.KEYBOARD_DEFAULT;
			break;
		case keyboardNumbers:
			tx.keyboardType = Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION;
			tx.returnKeyType = Titanium.UI.RETURNKEY_DONE;
			break;
		case keyboardNumberPad:
			tx.keyboardType = Titanium.UI.KEYBOARD_NUMBER_PAD;
			tx.returnKeyType = Titanium.UI.RETURNKEY_DONE;
			break;
	};

	//text Blur Event
	tx.addEventListener('blur', function(e) {
		//#78 如果是数字键盘类型，过滤用户输入的空格
		if(_args.keyboard == keyboardNumbers) {
			tx.value = RemoveAllSpace(e.value);
		}

		Titanium.App.Properties.setString('RowText', tx.value);
		Ti.API.info('row text finish  ');
		v.fireEvent('RowChange');
	});
	//
	//get local windows object
	var win = Titanium.UI.currentWindow;
	//text Focus Event
	tx.addEventListener('focus', function(e) {
		v.fireEvent('RowFocus');
		win.add(viewShadow);
	});
	//text Blue Event
	tx.addEventListener('blur', function(e) {
		v.fireEvent('RowLostFocus');
		win.remove(viewShadow);
	});
	//
	var v = wing.ui.createRow(_args);

	v.add(tx);

	return v;
};
//2011.11.1. #285 rowPicker object
//2011.11.4. #291 support picker single list
//2011.11.11. #317 add property: id，itemType
//2012.1.7. 输入时候防止其他输入控件
//2012.5.7. #53 #54 根据sdk 2.0.1 修改界面小问题
wing.ui.createRowPicker = function(/*Object*/_args) {

	//create tableview row
	var v = wing.ui.createRow(_args);

	//create Label
	var label = Ti.UI.createLabel({
		text : _args.value,
		height : 35,
		top : 5,
		right : 10,
		textAlign : 'right',
		width : 100
	});

	Titanium.App.Properties.setString('RowPicker', label.text);

	//get local windows object
	var win = Titanium.UI.currentWindow;

	var picker = Ti.UI.createPicker({
		bottom : 0,
		id : _args.id,
		itemType : 'PICKER'
	});

	//datetime picker
	if(_args.picker_type == pickerDate) {
		//create datepicker
		var minDate = new Date();
		minDate.setFullYear(2009);
		minDate.setMonth(0);
		minDate.setDate(1);

		var maxDate = new Date();
		maxDate.setFullYear(2013);
		maxDate.setMonth(11);
		maxDate.setDate(31);

		var value = new Date();

		picker.type = Ti.UI.PICKER_TYPE_DATE;
		picker.minDate = minDate;
		picker.maxDate = maxDate;
		picker.value = value;
	};

	//single list picker
	if(_args.picker_type == pickerSingle) {

		var column = Ti.UI.createPickerColumn();

		for( i = 0; i < _args.picker_data.length; i++) {
			column.addRow(Ti.UI.createPickerRow({
				title : _args.picker_data[i],
				custom_item : 'b'
			}));
		}
		picker.add(column);

		Ti.API.info('_args.value:' + _args.value);
		Ti.API.info('_args.picker_data:' + _args.picker_data);
		Ti.API.info('_agrs index:' + _args.picker_data.findIndex(_args.value));
	};

	// turn on the selection indicator (off by default)
	picker.selectionIndicator = true;

	//create ok button
	var btnOK = Ti.UI.createButton({
		title : _args.buttonOK_title || 'OK',
		height : 30,
		width : 80,
		font : {
			fontsize : 12
		},
		borderColor : 'white',
		backgroundColor : '#f37021',
		backgroundImage : 'NONE',
		selectedColor : 'blue',
		top : 120,
		left : 2
	});

	//Label click event
	label.addEventListener('click', function(e) {
		v.fireEvent('RowFocus');
		win.add(viewShadow);

		win.add(picker);
		win.add(btnOK);

		picker.setSelectedRow(0, _args.picker_data.findIndex(label.text), false);

		Ti.API.info('text click:' + _args.value);

		v.fireEvent('RowChange');
	});
	//button 'OK' click event
	btnOK.addEventListener('click', function(e) {
		win.remove(picker);
		win.remove(btnOK);
		Ti.API.info('picker close');
		v.fireEvent('PickerClose');

		v.fireEvent('RowLostFocus');
		win.remove(viewShadow);
	});
	//picker change event
	picker.addEventListener('change', function(e) {

		switch (_args.picker_type) {
			case pickerDate:
				var pickerdate = e.value;

				var day = pickerdate.getDate();
				day = day.toString();

				if(day.length < 2) {
					day = '0' + day;
				}

				var month = pickerdate.getMonth();
				month = month + 1;
				month = month.toString();

				if(month.length < 2) {
					month = '0' + month;
				}

				var year = pickerdate.getFullYear();
				label.text = year + "-" + month + "-" + day;

				break;

			case pickerSingle:
				var lp = picker.getSelectedRow(0).title;
				Ti.API.info('picker:' + lp);
				label.text = lp;
				Titanium.App.Properties.setString('RowPicker', lp);
				break;
		};

		v.fireEvent('RowChange');
	});

	v.add(label);

	return v;
};
//2011.11.1. #284 create tablerow object
wing.ui.createTableViewRow = function(/*Object*/_args) {

	switch (_args.type) {
		case rowSwitch:
			v = wing.ui.createRowSwitch(_args);
			break;
		case rowText:
			v = wing.ui.createRowText(_args);
			break;
		case rowPicker:
			v = wing.ui.createRowPicker(_args);
			break;
		case rowLabels:
			v = wing.ui.createRowLabels(_args);
			break;
	};

	return v;
};
//2012.1.3. #445 读取背景图案设置
//2012.3.8. #536
function GetBackgroundFile(dir_level) {
	var i = Titanium.App.Properties.getInt('User_BackgroundIndex');
	var lindex;
	if(i == null) {
		lindex = 1
	} else {
		lindex = i
	}
	if(dir_level == 2) {
		return '../../images/bg/back0' + lindex + '.jpg'
	} else {
		return '../images/bg/back0' + lindex + '.jpg'
	}
}