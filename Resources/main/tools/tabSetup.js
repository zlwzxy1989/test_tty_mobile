/*

 设置窗口

 2011.6.30. create
 2011.7.6. 增加电子邮件反馈
 2011.9.12. edit for v1.0.2 release
 2011.10.6. #194 修改win bar背景色
 2011.10.8. #224 所有调用browser显示内容的地方增加设置bar背景颜色
 tableviewSetup.addEventListener('click'...

 2011.11.26. #354 调整调用库函数方式
 2011.12.25. #436 修改为配置文件模式
 2012.2.4. #491 设置背景独立为一个文件，修改调用方式
 2012.2.24. #516 调整显示web调用方式
 2012.3.11. #543 调整工具和设置菜单
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

//2011.12.25. #436 修改为配置文件模式
var tableviewSetup = wing.ui.createTableView({
	type : tableviewSimple,
	ui_filename : 'tabSetup.ui',
	dir_level:2
})

//2012.3.8. #535 修改
function FeedBack() {
	var win = wing.ui.createWindow({
		url : 'tabFeedBack.js',
		title :'用户反馈',
		barColor : 'faa61a'
	});
	win.backgroundImage = GetBackgroundFile();
	Titanium.UI.currentTab.open(win, {
		animated : true
	});
}

//2012.1.3. 显示修改背景图案
//2012.2.4. #491 设置背景独立为一个文件，修改调用方式
function ShowChangeBackground() {
	var winBackground = Titanium.UI.createWindow({
		url : 'tabSetupBackground.js',
		title : '修改背景',
	})

	Titanium.UI.currentTab.open(winBackground, {
		animated : true
	})
}

// create table view event listener
//2011.10.8. #224 所有调用browser显示内容的地方增加设置bar背景颜色
//2011.12.25. #436 修改index值
//2012.2.4. #489
//2012.2.24. #516 调整显示web调用方式
//2012.3.8. #535
//2012.3.11. #543 调整工具和设置菜单
tableviewSetup.addEventListener('click', function(e) {

	switch (e.index) {
		case 0:
			ShowChangeBackground();
			break;
		case 1:
			FeedBack();
			break;
		case 2:
			Titanium.Platform.openURL('tel:4008213999');
			break;
	};
});
win.add(tableviewSetup);

//2012.2.4.
//windows lost focus
win.addEventListener('blur', function(e) {
	Ti.API.info('windows lost focus');
});
//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
