/*

2011.6.20. add ShowInfo()
2011.6.18. created
2011.10.4. #185 增加获得日期时间函数GetDateTime()
2011.10.4. #164 系统，每次启动app时候增加用户行为访问记录
2011.10.8. #223 调用浏览器可以设置标题栏颜色
2011.10.18. #241 行为记录函数PostBE加入appID，以适用多个应用
2011.10.31. #280 修改浏览器窗口标题栏可以不显示
2011.11.7. #304 tool.js 去掉网络连接检查，设置参数控制不显示出错信息
function PostBE() 修改相关调用
2011.11.8. #309 增加ForceLoad参数，设置在未联网状态强制读入web
#309 增加ForceLoad参数，设置在未联网状态强制读入web
2011.11.7. #304 tool.js 去掉网络连接检查，设置参数控制不显示出错信息
2011.11.27. #357 正则判断函数 function isRegExp(type, s)
2012.2.24. #516 系统，删除tools中原来的open web函数
2012.3.4. #522 postbe，修改本地调用方式,增加是否发送过判断
2012.5.7. add functions: ltrim rtrim trim getstrlength
2012.5.14 #84 POSTBE接口修改
2012.6.11. 增加 dateFormat函数
2012.8.1 #201 tools.js，修改stockColor函数
2012.8.3 增加函数getLableWidth
*/

//reg consts
regPostInteger = 1000;
regFloat = 1001;
//非负浮点数（正浮点数 + 0）

function CheckInternet(ShowWarn) {
	var Online;

	if(Titanium.Network.online == true) {
		Ti.API.info('online true');
		Online = true;
	} else {

		//如果参数为false，则不显示警告互联网链接问题窗口，无参数或者true显示警告
		if(ShowWarn == false) {
		} else {

			var alertOnline = Titanium.UI.createAlertDialog({
				title : '互联网连接状态'
			});

			alertOnline.buttonNames = null;
			alertOnline.message = '您的网络好像有问题，请检查一下互联网连接';
			alertOnline.show();
		}

		Ti.API.info('online false');
		Online = false;
	}
	return Online;
}

function ShowInfo(info) {

	var infowin = Titanium.UI.createWindow({
		height : 50,
		width : 250,
		bottom : 110,
		borderRadius : 10
	});

	var infoview = Titanium.UI.createView({
		backgroundColor : '#000',
		opacity : 0.5,
		height : 50,
		width : 250,
		borderRadius : 10
	});

	var infolabel = Titanium.UI.createLabel({
		color : '#fff',
		font : {
			fontSize : 18
		},
		textAlign : 'center',
		width : 'auto',
		height : 'auto'
	});
	infowin.add(infoview);
	infowin.add(infolabel);

	infolabel.text = info;
	infowin.open();
	setTimeout(function() {
		infowin.close({
			opacity : 0,
			duration : 500
		});
	}, 2000);
}

//2011.6.21.
function GetPhoneType() {
	return Titanium.Platform.model;
}

//2011.6.21.
//2012.5.22.
function GetPhoneTimeout() {

	Ti.API.info('Phone:' + GetPhoneType());

	if(GetPhoneType() == 'iPhone 4') {
		return 3000;
	} else if(GetPhoneType().indexOf('iPad') > -1) {
		return 3000;
	} else if(Titanium.Platform.name == 'android') {
		return 3000;
	} else {
		return 4000;
	}
};

Array.prototype.inArray = function(value)
// Returns true if the passed value is found in the
// array.  Returns false if it is not.
{
	var i;
	for( i = 0; i < this.length; i++) {
		// Matches identical (===), not just similar (==).
		if(this[i] === value) {
			return true;
		}
	}
	return false;
};

Array.prototype.del = function(n) {//n表示第几项，从0开始算起。
	//prototype为对象原型，注意这里为对象增加自定义方法的方法。
	if(n < 0)//如果n<0，则不进行任何操作。
	{
		return this;
	} else {
		return this.slice(0, n).concat(this.slice(n + 1, this.length));
	}
	/*
	 concat方法：返回一个新数组，这个新数组是由两个或更多数组组合而成的。
	 　　　　　　       这里就是返回this.slice(0,n)/this.slice(n+1,this.length)
	 　　　　　　       组成的新数组，这中间，刚好少了第n项。
	 slice方法：   返回一个数组的一段，两个参数，分别指定开始和结束的位置。
	 */
};
//查找数组元素, 返回元素下标
Array.prototype.findIndex = function(value) {
	var index = -1;
	for(var i = 0; i < this.length; i++) {
		if(this[i] === value) {
			index = i;
			break;
		}
	}
	return index;
};
//删除数组元素, 并返回元素下标
Array.prototype.deleteByValue = function(value) {
	var index = this.findIndex(value);
	if(index > -1) {
		this.splice(index, 1);
	}
	return index;
};
//2011.12.23.
String.prototype.removeAllSpace = function() {
	return this.replace(/\s/g, '');
}
//2012.5.16.
function RemoveAllSpace(str) {
	return str.replace(/\s/g, '');
}

//计算含有全角字符的字符串的准确长度
function CnGetLen(str) {
	return str.replace(/[^\x00-\xff]/g, '**').length;
}

//获得含有中文字符的子串，并加上...
function CnSubString(str, len, hasDot) {
	var newLength = 0;
	var newStr = "";
	var chineseRegex = /[^\x00-\xff]/g;
	var singleChar = "";
	var strLength = str.replace(chineseRegex, "**").length;
	for(var i = 0; i < strLength; i++) {
		singleChar = str.charAt(i).toString();
		if(singleChar.match(chineseRegex) != null) {
			newLength += 2;
		} else {
			newLength++;
		}
		if(newLength > len) {
			break;
		}
		newStr += singleChar;
	}

	if(hasDot && strLength > len) {
		newStr += "...";
	}
	return newStr;
}

// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.
function CSVToArray(strData, strDelimiter) {
	// Check to see if the delimiter is defined. If not,
	// then default to comma.
	strDelimiter = (strDelimiter || ",");

	// Create a regular expression to parse the CSV values.
	var objPattern = new RegExp((
		// Delimiters.
		"(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

		// Quoted fields.
		"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

		// Standard fields.
		"([^\"\\" + strDelimiter + "\\r\\n]*))"
	), "gi");

	// Create an array to hold our data. Give the array
	// a default empty first row.
	var arrData = [[]];

	// Create an array to hold our individual pattern
	// matching groups.
	var arrMatches = null;

	// Keep looping over the regular expression matches
	// until we can no longer find a match.
	while( arrMatches = objPattern.exec(strData)) {

		// Get the delimiter that was found.
		var strMatchedDelimiter = arrMatches[1];

		// Check to see if the given delimiter has a length
		// (is not the start of string) and if it matches
		// field delimiter. If id does not, then we know
		// that this delimiter is a row delimiter.
		if(strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {

			// Since we have reached a new row of data,
			// add an empty row to our data array.
			arrData.push([]);

		}

		// Now that we have our delimiter out of the way,
		// let's check to see which kind of value we
		// captured (quoted or unquoted).
		if(arrMatches[2]) {

			// We found a quoted value. When we capture
			// this value, unescape any double quotes.
			var strMatchedValue = arrMatches[2].replace(new RegExp("\"\"", "g"), "\"");

		} else {

			// We found a non-quoted value.
			var strMatchedValue = arrMatches[3];

		}

		// Now that we have our value string, let's add
		// it to the data array.
		arrData[arrData.length - 1].push(strMatchedValue);
	}

	// Return the parsed data.
	return (arrData )
}

function formatNumber(num, pattern) {
	var strarr = num ? num.toString().split('.') : ['0'];
	var fmtarr = pattern ? pattern.split('.') : [''];
	var retstr = '';

	// 整数部分
	var str = strarr[0];
	var fmt = fmtarr[0];
	var i = str.length - 1;
	var comma = false;
	for(var f = fmt.length - 1; f >= 0; f--) {
		switch(fmt.substr(f,1)) {
			case '#':
				if(i >= 0) {
					retstr = str.substr(i--, 1) + retstr;
				}
				break;
			case '0':
				if(i >= 0) {
					retstr = str.substr(i--, 1) + retstr;
				} else {
					retstr = '0' + retstr;
				}
				break;
			case ',':
				comma = true;
				retstr = ',' + retstr;
				break;
		}
	}
	if(i >= 0) {
		if(comma) {
			var l = str.length;
			for(; i >= 0; i--) {
				retstr = str.substr(i, 1) + retstr;
				if(i > 0 && ((l - i) % 3) == 0) {
					retstr = ',' + retstr;
				}
			}
		} else {
			retstr = str.substr(0, i + 1) + retstr;
		}
	}
	retstr = retstr + '.';
	// 处理小数部分
	str = strarr.length > 1 ? strarr[1] : '';
	fmt = fmtarr.length > 1 ? fmtarr[1] : '';
	i = 0;
	for(var f = 0; f < fmt.length; f++) {
		switch(fmt.substr(f,1)) {
			case '#':
				if(i < str.length) {
					retstr += str.substr(i++, 1);
				}
				break;
			case '0':
				if(i < str.length) {
					retstr += str.substr(i++, 1);
				} else {
					retstr += '0';
				}
				break;
		}
	}
	return retstr.replace(/^,+/, '').replace(/\.$/, '');
}

//2011.7.5.
//2012.3.10. #542
function getDate() {
	var currentTime = new Date();

	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();

	return formatNumber(year, '0000') + "-" + formatNumber(month, '00') + "-" + formatNumber(day, '00');
}

//#185 增加获得日期时间函数GetDateTime()
//2011.10.4.
function getDateTime() {
	var currentTime = new Date();

	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();

	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();

	return year + "-" + month + "-" + day + ' ' + hours + ':' + minutes + ':' + seconds;

}

//2011.12.17. 加入非负浮点数判断
//2011.11.27. create
//#357 正则判断函数
function isRegExp(type, s) {
	var patt1 = new RegExp("^[0-9]*[1-9][0-9]*$");
	switch (type) {
		case regPostInteger:
			patt1 = new RegExp("^[0-9]*[1-9][0-9]*$");
			break;
		case regFloat:
			patt1 = new RegExp("^\\d+(\\.\\d+)?$");
			break;
	};
	return patt1.test(s);
}

//2011.12.27.
//2012.8.1 #201 tools.js，修改stockColor函数
function stockColor(value) {
	var lcolor, lgreen;

	if(Ti.Platform.name == "iPhone OS") {
		lgreen = '669966'
	} else {
		lgreen = 'green'
	}
	lcolor = 'gray';

	if(value.substring(0, 1) == '-') {
		lcolor = lgreen
	} else if(value == '0.00%' || value == 0) {
		lcolor = 'gray';
	} else {
		lcolor = 'red';
	}

	return lcolor;
}

//2012.4.23 function for data api
function getApiUrl(_arr/* Array */) {
	var KEY = 'TTYFUND-CHINAPNR';
	arr_temp = [];
	//存放调用接口GET部分内容的临时数组
	//过滤不符合条件的传入参数
	if(_arr['act'] == null) {
		return false;
	}
	if(_arr['key'] == null || _arr['key'] != KEY) {
		return false;
	}
	if(_arr['url'] == null) {
		return false;
	}
	var url = _arr['url'];
	_arr['url'] = null;
	//拼接参数组成调用地址
	for(key in _arr) {
		if( typeof (_arr[key]) != 'string' && typeof (_arr[key]) != 'number')
			continue;
		value = _arr[key];
		arr_temp.push(key + '=' + value);
	}
	url = url + '?' + arr_temp.join('&');

	return url;

}

//delete left & right space
function LTrim(str) {
	var i;
	for( i = 0; i < str.length; i++) {
		if(str.charAt(i) != "\n" && str.charAt(i) != " ")
			break;
	}
	str = str.substring(i, str.length);
	return str;
}

function RTrim(str) {
	var i;
	for( i = str.length - 1; i >= 0; i--) {
		if(str.charAt(i) != "\n" && str.charAt(i) != " ")
			break;
	}
	str = str.substring(0, i + 1);
	return str;
}

function Trim(str) {
	return LTrim(RTrim(str));
}

function getStrLength(str) {
	str = Trim(str);
	var chineseRegex = /[^\x00-\xff]/g;
	var strLength = str.replace(chineseRegex, "**").length;
	return strLength;
}

/*格式化日期字符串
 * 传入：字符串，格式为YYYYMMDD
 * 返回：字符串，格式为YYYY-MM-DD
 * */
function dateFormat(dateStr) {
	dateStr = dateStr + '';
	return dateStr.substring(0, 4) + '-' + dateStr.substring(4, 6) + '-' + dateStr.substring(6, 8);
}

//根据字符串的长度计算UI的宽度 by zxy
//传入：字符串
//返回：宽度
function getLableWidth(text) {
	if(CnGetLen(text) > 0) {
		return CnGetLen(text) * 10;
	} else
		return 0;
}

//根据传入的tableView的style返回相应的style by zxy
//因Titanium.UI.iPhone.TableViewStyle.PLAIN为0，要将默认设为Titanium.UI.iPhone.TableViewStyle.GROUPED不能使用三元表达式
//传入：style
//返回：style
function getTableViewStyle(num) {
	var style;
	if(num != undefined) {
		style = num;
	} else {
		style = Titanium.UI.iPhone.TableViewStyle.GROUPED;
	}
	return style;
}

//根据传入的UI种类和UI参数创建不同的UI by zxy
// 传入： _args_ui 参数数组 text 填入UI的文本
// 返回：无
function createUiInTableView(_args_ui,text) {
	switch (_args_ui.type) {
		//简单tabelViewRow
		case "rowSimple":
			var row = Ti.UI.createTableViewRow();
			row.height = _args_ui.height || 43;
			row.title = _args_ui.title;
			if(_args_ui.dir_level == 2) {
				row.leftImage = '../../images/tableview/' + _args_ui.leftImage
			} else {
				row.leftImage = '../images/tableview/' + _args_ui.leftImage
			}
			row.backgroundColor = _args_ui.backgroundColor || 'transparent';
			row.hasChild = _args_ui.hasChild || false;
			row.top = _args_ui.top || 0;
			row.color = _args_ui.color || 'black';
			row.size = _args_ui.size || 14;
			if(_args_ui.header != undefined) {
				row.header = _args_ui.header;
			}
			// rss
			if(_args_ui.url != undefined) {
				if(Ti.Platform.name == "iPhone OS") {
					row.url = _args_ui.url || "";
				} else if(Ti.Platform.name == "android") {
					row.className = _args_ui.url || "";
				}
			}
			return row;
			break;
		//复杂tabelViewRow
		case "rowComplex":
			var row = Ti.UI.createTableViewRow();
			row.height = _args_ui.height || 43;
			row.width = _args_ui.width || '100%';
			row.zIndex = _args_ui.zIndex || 100;
			row.hasChild = _args_ui.hasChild || false;
			row.backgroundColor = _args_ui.backgroundColor || 'transparent';
			if(_args_ui.header != undefined) {
				row.header = _args_ui.header;
			}
			if(_args_ui.height != undefined && _args_ui.height == "Ti.UI.SIZE") {
				row.height = Ti.UI.SIZE;
			}
			return row;
			break;
		//textArea
		case "textArea":
			var ltext = text;
			Ti.API.info('textArea text: ' + ltext);
			var textArea = Ti.UI.createTextArea({
				value : ltext || '',
				backgroundColor : _args_ui.backgroundColor || 'transparent',
				textAlign : _args_ui.backgroundColor || Ti.UI.TEXT_ALIGNMENT_LEFT,
				width : _args_ui.width || '100%',
				font : {
					fontSize : _args_ui.fontSize || 14,
					fontWeight : _args_ui.fontWeight || 'normal'
				},
				borderWidth : _args_ui.borderWidth || 0,
				borderRadius : _args_ui.borderRadius || 0,
				borderColor : _args_ui.borderColor || 'transparent',
				top : _args_ui.top || 0,
				height : _args_ui.height || Ti.UI.SIZE,
				editable : _args_ui.editable || false,
				scrollable : _args_ui.scrollable || false,
			});

			setUiLayout(textArea, _args_ui, ltext);
			return textArea;
		//label使用最多，设为默认
		default:
			var ltext, lcolor;
			ltext = text;
			lcolor = _args_ui.color || 'black';
			Ti.API.info('label text: ' + ltext);
			if(lcolor === 'stock') {
				lcolor = stockColor(ltext);
			}
			//create labels
			var label = Ti.UI.createLabel({
				text : ltext,
				color : lcolor,
				backgroundColor : _args_ui.backgroundColor || 'transparent',
				textAlign : _args_ui.textAlign || Ti.UI.TEXT_ALIGNMENT_LEFT,
				//top : _args_ui.top || 0,
				//left : _args_ui.left + _args.offset || 0, //offset for android
				height : _args_ui.height || 43,
				width : _args_ui.width || 0,
				clickname : _args_ui.clickname || '',
				zIndex : _args_ui.zIndex || 1,
				font : {
					fontWeight : _args_ui.fontWeight || 'normal',
					fontSize : _args_ui.fontSize || 14,
					fontFamily : 'Arial'
				}
			});
			setUiLayout(label, _args_ui, ltext);
			return label;
			break;
	}
}

//为兼容android，ios和android设置不同的top、bottom、left、right等参数，并计算width、height by zxy
//传入：UI UI元素 _args_ui ui配置参数 ltext 显示在UI中的字符串
//传出：无
function setUiLayout(UI, _args_ui, ltext) {
	//兼容android
	if(Ti.Platform.name == "iPhone OS") {
		if(_args_ui.top != undefined) {
			UI.top = _args_ui.top;
		}
		if(_args_ui.bottom != undefined) {
			UI.bottom = _args_ui.bottom;
		}
		if(_args_ui.left != undefined) {
			UI.left = _args_ui.left;
		}
		if(_args_ui.right != undefined) {
			UI.right = _args_ui.right;
		}
	} else if(Ti.Platform.name == "android") {
		UI.top = _args_ui.top || 'auto';
		UI.bottom = _args_ui.bottom || 'auto';
		UI.left = _args_ui.left + _args.offset || 'auto';
		UI.right = _args.right || 'auto';
	}
	//若未设置宽度，根据字符串长度，计算宽度
	if(_args_ui.width == undefined) {
		UI.width = getLableWidth(ltext);
	}
	//设置特殊高度
	//根据文字内容自动调整高度
	if(_args_ui.height != undefined && _args_ui.height == "Ti.UI.SIZE") {
		UI.height = Ti.UI.SIZE;
	}
	//基金经理页面的textArea高度计算
	else if(_args_ui.height == "!FUND_MANAGER") {
		//每行字数
		var WORD_COUNT = 40;
		//字高
		var WORD_HEIGHT = 18;
		//行数
		var lines = Math.ceil(getStrLength(ltext) / WORD_COUNT);
		//多空出一些便于美观
		UI.height = (lines * WORD_HEIGHT) + 15;
	}
}