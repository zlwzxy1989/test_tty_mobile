/*

 FileName: tabIndexDetail.js
 Function:显示国内主要证券指数

 2011.12.11. create #378, #379 基本信息显示
 2012.7.3.	#182 理财工具，国内证券指数行情，读取天天盈网站接口避免出现乱码
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');

//
var my = {};

my.data = [];

var win = Titanium.UI.currentWindow;

win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);

var IndexData = Titanium.App.Properties.getList('IndexData');
var IndexRowIndex = Titanium.App.Properties.getInt('IndexRowIndex');
Ti.API.info('load indexdata:' + IndexData);
Ti.API.info('table view row index:' + IndexRowIndex);

//获取当前大盘的数据
var data = IndexData[IndexRowIndex];

var Index = formatNumber(data["index_value"], '#,###.000');

//今日开盘
var IndexTodayOpen = formatNumber(data["index_today"], '#,###.000');

//昨日收盘
var IndexLastClose = formatNumber(data["index_yesterday"], '#,###.000');

//涨跌
var IndexChange = data["index_change"];

//涨跌率
var IndexRatio = data["index_rate"];

//今日最高
var IndexTodayHigh = formatNumber(data["index_max"], '#,###.000');

//今日最低
var IndexTodayLow = formatNumber(data["index_min"], '#,###.000');

//成交笔数
var IndexCount = formatNumber(data["index_amount"], '#,###');

//成交金额
var IndexMoney = formatNumber(data["index_money"] / 10000, '#,###.00');

//日期
var IndexDate = data["index_time"];

my.data = [];
my.data.push('当前指数：' + Index);
my.data.push('');
my.data.push('涨跌值：' + IndexChange);
my.data.push('涨跌幅：' + IndexRatio);
my.data.push('今开：' + IndexTodayOpen);
my.data.push('昨收：' + IndexLastClose);
my.data.push('最高：' + IndexTodayHigh);
my.data.push('最低：' + IndexTodayLow);
my.data.push('成交量：' + IndexCount + ' 手');
my.data.push('');
my.data.push('成交金额：' + IndexMoney + ' 万');
my.data.push('');

var lblUpdate = wing.ui.createLabel({
	color : 'white',
	backgroundColor : 'transparent',
	bottom : 10,
	left : 60,
	width:300,
	height:30
});

var tableviewCreate = false;
var imageCreate = false;

var lblHint = wing.ui.createLabel({
	text : '可以拖动来显示分时线图和日K线图',
	color : 'darkgray',
	backgroundColor : 'transparent',
	bottom : 10
});

var viewTableview = Ti.UI.createView({
	backgroundColor : 'transparent'
});

win.add(lblUpdate);
win.add(viewTableview);

//2012.1.10.
function ShowIndexDetail() {

	if(tableviewCreate) {
		win.remove(tableviewIndex)
	};

	//2011.12.27. 修改为配置文件模式
	tableviewIndex = wing.ui.createTableView({
		type : tableviewLabels,
		value : my.data,
		ui_filename : 'tabIndexDetail.ui'
	})
	tableviewCreate = true;

	lblUpdate.text = '更新时间：' + IndexDate;

	win.add(tableviewIndex);

}

//如果联网状态
if(CheckInternet()) {
	ShowIndexDetail();
}

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
