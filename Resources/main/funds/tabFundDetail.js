/*

 FileName: tabFundDetail.js
 Function:显示指定基金的明细

 ----
 2011.6.10.
 2011.6.11. post the data
 2011.6.12. edit for new interface
 2011.6.16. 优化和修改错误
 2011.6.17. 优化和修改错误，修改row显示的颜色字体
 2011.6.22. 优化
 2011.6.23. 优化 #37
 2011.6.24. 自定义基金功能开始
 2011.6.25. 自定义基金功能
 2011.7.5. 限制自定义基金为10个
 2011.8.20. 修改基金列表呈现为myui方式
 2011.9.29. #177 基金内页，将“收益率”修改为“日增幅”
 2011.9.30. #176 基金内页，根据myui修改来调整显示，调整字体大小
 2011.10.6. #194 修改win bar背景色
 2011.10.8. #222 基金内页，增加背景图相关修改
 2011.11.26. #354 调整调用库函数方式
 2011.12.21. #413 xhr读入方式
 2011.12.24. #432 基金详细页，增加基金净值日线图
 2011.12.23. #424 防止用户自定义和删除基金按钮重复点击
 2012.2.4. #498 基金详细页，标题显示不占用tableview
 2012.2.5. #497 基金详细页，基金图表视图对于长基金名称的标题会折行
 2012.2.5. #496 基金详细页，显示可以拖动的话语会在拖动时有闪烁感
 2012.4.28. ac #41 基金列表，tableview中label高度有问题
 2012.5.8. ac #68 基金详细页，删除新浪基金历史数据图
 2012.5.30. ac #112 基金内页，首页去除显示最近四天净值的代码并保证关注基金功能正常
 2012.5.30. ac #102 基金内页，首页读取基金当天净值信息、基金所属基金公司的tag的ID
 2012.5.30. ac #103 基金内页，首页根据基金种类显示不同的内容
 2012.6.6. ac #111 基金内页，根据基金所属基金公司读取相关新闻
 2012.6.8 ac #117 基金内页，没有基金的名称和代码
 2012.6.8. ac #119 基金内页，走势图详情页图片可拖动
 2012.6.12. ac #122 基金内页，基金基金信息排版修改
 2012.6.12. ac #123 基金内页，更新时间需显示在一屏内
 2012.6.12. ac #125 走势图详情，没有显示基金名称
 2012.6.12. ac #126 基金内页，相关新闻列表进入后头部显示“XX基金相关新闻”
 2012.6.12. ac #124 基金内页，走势图不应与收益信息等分为一栏
 2012.6.12. ac #127 基金内页，冒号之间间距不一致
 2012.6.12. ac #128 基金内页，基金名称与基金代码在头部固定
 2012.6.18. ac #133 基金内页，更新日期栏目调整到第一行
 2012.6.18. ac #140 基金内页，二级菜单详情页如“基金详情”详情页，首行添加“基金详情”
 2012.6.18. ac #142 基金内页，有二级目录的标题与新闻列表格式统一
 2012.6.24. #161: 基金内页，基金走势图栏目改为历史净值
 2012.6.24.	#162: 基金内页，基金单日净值走势图作为二级菜单，不直接显示在基金内页
 2012.6.24.	#163: 基金内页，货币基金的“基金走势图”改为“历史收益”
 2012.6.24.	#164: 基金内页，“基金历史净值”包括“基金历史净值”、“单位净值走势图”和“累积净值走势图”
 2012.6.24. #165: 基金内页，基金历史净值从基金相关数据中去除
 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
 */

Ti.include('../../tools/tools.js');
Ti.include('../../tools/wing_ui.js');
Ti.include('../../tty_tools/consts.js');

var win = Titanium.UI.currentWindow;
//#194 修改win bar背景色
win.barColor = 'faa61a';
win.backgroundImage = GetBackgroundFile(2);
//检查网络连接
CheckInternet();
//win.title = '基金详情';
var UserFundCode = Titanium.App.Properties.getString('UserFundCode');
Titanium.API.info('i am fundnet window, user chose : ' + UserFundCode);

var UD_FundCount;
var UD_FundCode = [];

var tempFundNet, tempFundReturn;

var xmlstr;

var my = {};

my.xhr = Titanium.Network.createHTTPClient();

var btnUserDefine = Titanium.UI.createButton({
	image : '../../images/system/btn_remove_fund.png'
});

var btnUserRemove = Titanium.UI.createButton({
	image : '../../images/system/btn_add_fund.png'
});

//2011.10.8. #222 基金内页，增加背景图相关修改
var data = [];

var FundName, FundCode, FundCount;
//2011.12.23. #424 防止用户自定义和删除基金按钮重复点击
//删除自定义基金按钮
btnUserRemove.addEventListener('click', function(e) {

	win.rightNavButton = null;

	Ti.API.info('UD code:' + UD_FundCode);
	//数组中删除
	UD_FundCode.deleteByValue(UserFundCode);

	Ti.API.info('UD code:' + UD_FundCode);

	//计数器删除
	UD_FundCount = UD_FundCount - 1;

	//数组和计数器存盘
	Titanium.App.Properties.setInt('UD_FundCount', UD_FundCount);
	Ti.API.info('User Define Fund Count now is:' + UD_FundCount);

	Titanium.App.Properties.setList('UD_FundCode', UD_FundCode);

	ShowInfo('已经从关注基金中删除');

	win.rightNavButton = btnUserDefine;

	//设置UD强制刷新
	Titanium.App.Properties.setBool('UD_NeedRefresh', true);

});

//2011.7.5. 增加判断自定义基金数量
//2011.12.23. #424 防止用户自定义和删除基金按钮重复点击
//2012.2.12. 常量
//增加自定义基金按钮
btnUserDefine.addEventListener('click', function(e) {

	win.rightNavButton = null;

	Ti.API.info('User Define Fund Count is:' + UD_FundCount);

	if(UD_FundCount === ud_max) {

		ShowInfo('您的关注基金数量已经到了' + ud_max + '个的限制');

	} else {

		//用户没有自定义过，初始化
		if((UD_FundCount === null) || (UD_FundCount === 0)) {
			UD_FundCount = 0;
		};

		Ti.API.info('ud want:' + UserFundCode + ' ' + FundName);
		UD_FundCode[UD_FundCount] = UserFundCode;

		Ti.API.info('ud FundCode:' + UD_FundCode[UD_FundCount]);
		UD_FundCount++;
		Titanium.App.Properties.setInt('UD_FundCount', UD_FundCount);
		Ti.API.info('User Define Fund Count now is:' + UD_FundCount);

		Titanium.App.Properties.setList('UD_FundCode', UD_FundCode);

		ShowInfo('已经添加到关注基金');

		win.rightNavButton = btnUserRemove;

		//设置UD强制刷新
		Titanium.App.Properties.setBool('UD_NeedRefresh', true);

	}

});

//获得用户自定义基金数量
UD_FundCount = Titanium.App.Properties.getInt('UD_FundCount');
UD_FundCode0 = Titanium.App.Properties.getList('UD_FundCode');

if(UD_FundCode0 == null) {
} else {
	for( i = 0; i < UD_FundCode0.length; i++) {
		UD_FundCode[i] = UD_FundCode0[i];
	}
}
Ti.API.info('fundcode:' + UserFundCode);
//Ti.API.info('')
Ti.API.info('ud_FundCode:' + UD_FundCode);
//检查显示的基金是否已经存在自定义中
if(UD_FundCode.inArray(UserFundCode)) {
	Ti.API.info('fund code in ud');
	win.rightNavButton = btnUserRemove;

} else {
	Ti.API.info('fund code not in ud');
	win.rightNavButton = btnUserDefine;
}
//以下基金内页部分 by zxy
//2012.5.30. ac #102 基金内页，首页读取基金当天净值信息、基金所属基金公司的tag的ID
Titanium.App.Properties.setString('fund_code_detail', UserFundCode);
var my = {};
//调用基金详细信息接口，获取净值、日涨幅（七日年化、万份收益）
my.getFundDetail = Ti.Network.createHTTPClient();
var arr_getFundDetail = {};
var json_getFundDetail = {};
arr_getFundDetail.key = 'TTYFUND-CHINAPNR';
arr_getFundDetail.act = 'GetFundInfo';
arr_getFundDetail.symbol = UserFundCode;
arr_getFundDetail.url = 'http://www.ttyfund.com/api/services/FundService.php';
url_getFundDetail = getApiUrl(arr_getFundDetail);
Ti.API.info('url_getFundDetail:' + url_getFundDetail);
my.getFundDetail.open("GET", url_getFundDetail);
my.getFundDetail.onload = function() {
	try {
		json_getFundDetail = JSON.parse(my.getFundDetail.responseText);
		if(json_getFundDetail.return_code == 1) {
			//是否有该基金公司对应的tagID。如果没有，则新闻资讯行不可点（新上线的基金公司可能更新新闻会有延迟）
			var hasNews;
			//基金名称
			fund_name = json_getFundDetail.display_short_name_local;
			//2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
			Titanium.App.Properties.setString('fund_name_detail', fund_name);
			//基金类型
			fund_type = json_getFundDetail.category_local;
			Ti.API.info("fund type: " + fund_type);
			//是否是货币基金，货币和非货币需要分开显示
			isCurrency = false;
			if(fund_type == '货币型A' || fund_type == '货币型B') {
				isCurrency = true;
			}
			//保存判断信息，二级页面可能用到
			Titanium.App.Properties.setBool('isCurrency', isCurrency);
			//基金公司名称，用于对应新闻的tagID
			FundCompany = json_getFundDetail.com_alias;
			//tableView数据数组
			my.data = [];
			if(isCurrency) {
				//货币型不需要获取各期间涨幅，直接获取基金所属基金公司对应的tagID
				getCompanyTag();
			} else {
				//非货币型，需要继续获取各期间涨幅数据
				my.getFundReturns = Ti.Network.createHTTPClient();
				var arr_getFundReturns = {};
				var json_getFundReturns = {};
				arr_getFundReturns.key = 'TTYFUND-CHINAPNR';
				arr_getFundReturns.act = 'GetRelativeFund';
				arr_getFundReturns.symbol = UserFundCode;
				arr_getFundReturns.url = 'http://www.ttyfund.com/api/services/FundService.php';
				url_getFundReturns = getApiUrl(arr_getFundReturns);
				Ti.API.info('url_getFundReturns:' + url_getFundReturns);
				my.getFundReturns.open("GET", url_getFundReturns);
				my.getFundReturns.onload = function() {
					try {
						Ti.API.info(my.getFundReturns.responseText);
						json_getFundReturns = JSON.parse(my.getFundReturns.responseText);
						if(json_getFundReturns.return_code == 1) {
							var returns = {};
							//各期间涨幅数据
							returns = json_getFundReturns.me;
							Ti.API.info('returns: ' + returns);
							//获取基金所属基金公司对应的tagID
							getCompanyTag(returns);
						}
					} catch(E) {
						Ti.API.info(E);
					}
				}
				my.getFundReturns.send();
			}
		}
	} catch(E) {
		Ti.API.info(E);
	}
}
my.getFundDetail.send();
/* 获取基金公司对应tag
 * 传入：涨幅数组（不传入的话无法读取同个onload函数下http请求返回的数据）
 * 传出：无
 */
function getCompanyTag(returns) {
	my.getCompanyTag = Ti.Network.createHTTPClient();
	var json_getCompanyTag = {};
	url_getCompanyTag = 'http://www.ttyfund.com/api/mobile/companyToTag.txt';
	my.getCompanyTag.open("GET", url_getCompanyTag);
	my.getCompanyTag.onload = function() {
		try {
			Ti.API.info(my.getCompanyTag.responseText);
			json_getCompanyTag = JSON.parse(my.getCompanyTag.responseText);
			if(json_getCompanyTag[FundCompany] != null) {
				Ti.API.info('company tag id: ' + json_getCompanyTag[FundCompany]);
				//记录基金公司对应的tagID
				Titanium.App.Properties.setInt('companyTag', json_getCompanyTag[FundCompany]);
				hasNews = true;
			} else {
				hasNews = false;
			}
			//获取tagID后生成、显示tableView
			showTableView(returns);
		} catch(E) {
			Ti.API.info(E);
		}
	}
	my.getCompanyTag.send();
}

/* 生成、显示tableView
* 传入：涨幅数组（不传入的话无法读取同个onload函数下http请求返回的数据）
* 传出：无
*/
//2012.5.30. ac #103 基金内页，首页根据基金种类显示不同的内容
function showTableView(returns) {
	//2012.6.12. ac #128 基金内页，基金名称与基金代码在头部固定
	if(getStrLength(fund_name) > 20) {
		fund_name = CnSubString(fund_name, 19, true);
	}
	labelTitle = Ti.UI.createLabel({
		text : fund_name + '\r' + UserFundCode,
		height : 43,
		color : 'white',
		textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
		width : '100%',
		font : {
			fontSize : 14,
			fontWeight : 'bold'
		},
	});
	win.setTitleControl(labelTitle);
	// 2012.8.2 ＃196 优化范围内页面调用wing.ui.createTableView部分修改 by zxy
	if(isCurrency) {
		//货币基金
		my.data.push(dateFormat(json_getFundDetail.price_date));
		my.data.push(parseFloat(json_getFundDetail.day_price).toFixed(4));
		my.data.push(parseFloat(json_getFundDetail.rating).toFixed(2) + '%');
		var ui_filename = "tabFundDetailCurrency.ui";
	} else {
		//非货币基金
		my.data.push(parseFloat(json_getFundDetail.day_price).toFixed(4));
		my.data.push(dateFormat(json_getFundDetail.price_date));
		my.data.push(parseFloat(json_getFundDetail.rating).toFixed(2) + '%');
		my.data.push(parseFloat(returns.return6month).toFixed(2) + '%');
		my.data.push(parseFloat(returns.return1month).toFixed(2) + '%');
		my.data.push(parseFloat(returns.return1year).toFixed(2) + '%');
		my.data.push(parseFloat(returns.return3month).toFixed(2) + '%');
		my.data.push(parseFloat(returns.returnytd).toFixed(2) + '%');
		var ui_filename = "tabFundDetailNoCurrency.ui";
	}
	tableviewFund = wing.ui.createTableView({
		backgroundColor : 'transparent',
		type : tableviewComplex,
		value : my.data,
		ui_filename : ui_filename
	});
	//根据是否有该基金公司的新闻确定是否显示右侧箭头
	section = tableviewFund.data[tableviewFund.data.length - 1];
	section.rows[section.rowCount - 1].hasChild = hasNews;
	//非货币基金的click事件响应函数
	noCurrencyListner = function(e) {
		Ti.API.info(e.index + " clicked");
		title = e.row.title;
		// 2012.6.18. ac #140 基金内页，二级菜单详情页如“基金详情”详情页，首行添加“基金详情”
		Titanium.App.Properties.setString('title_detail', title);
		switch (e.index) {
			// 2012.6.18. ac #133 基金内页，更新日期栏目调整到第一行
			//基金历史净值
			case 4:
				Titanium.App.Properties.setString('line_type', 'nav');
				openWindow('tabFundDetailHistory.js');
				break;
			//基金净值走势
			case 5:
				Titanium.App.Properties.setString('line_type', 'nav');
				// 2012.6.12. ac #125 走势图详情，没有显示基金名称
				title = fund_name;
				openWindow('tabFundDetailLines.js');
				break;
			//基金累计净值走势
			case 6:
				Titanium.App.Properties.setString('line_type', 'accm_nav');
				// 2012.6.12. ac #125 走势图详情，没有显示基金名称
				title = fund_name;
				openWindow('tabFundDetailLines.js');
				break;
			// 2012.6.24. #165: 基金内页，基金历史净值从基金相关数据中去除
			//基金基本信息
			case 7:
				openWindow('tabFundDetailInfo.js');
				break;
			//基金分红
			case 8:
				Titanium.App.Properties.setString('line_type', 'fenhong');
				openWindow('tabFundDetailHistory.js');
				break;
			//基金拆分
			case 9:
				Titanium.App.Properties.setString('line_type', 'chaifen');
				openWindow('tabFundDetailHistory.js');
				break;
			//基金经理信息
			case 10:
				openWindow('tabFundDetailManager.js');
				break;
			//新闻资讯
			case 11:
				if(hasNews == true) {
					Titanium.App.Properties.setString('news_display_type', 'fund_detail');
					//2012.6.12. ac #126 基金内页，相关新闻列表进入后头部显示“XX基金相关新闻”
					title = FundCompany + '基金相关资讯';
					//2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
					openWindow('../news/tabNewsList.js', true);
				}
				break;
		}
	}
	//货币基金的click事件响应函数
	currencyListner = function(e) {
		Ti.API.info(e.index + " clicked");
		// 2012.6.18. ac #142 基金内页，有二级目录的标题与新闻列表格式统一
		title = e.row.title;
		// 2012.6.18. ac #140 基金内页，二级菜单详情页如“基金详情”详情页，首行添加“基金详情”
		Titanium.App.Properties.setString('title_detail', title);
		switch (e.index) {
			//7日年化走势
			// 2012.6.18. ac #133 基金内页，更新日期栏目调整到第一行
			case 2:
				Titanium.App.Properties.setString('line_type', 'yield');
				// 2012.6.12. ac #125 走势图详情，没有显示基金名称
				title = fund_name;
				openWindow('tabFundDetailLines.js');
				break;
			//万份收益走势
			case 3:
				Titanium.App.Properties.setString('line_type', 'gain');
				// 2012.6.12. ac #125 走势图详情，没有显示基金名称
				title = fund_name;
				openWindow('tabFundDetailLines.js');
				break;
			//基金基本信息
			case 4:
				openWindow('tabFundDetailInfo.js');
				break;
			//基金经理信息
			case 5:
				openWindow('tabFundDetailManager.js');
				break;
			//新闻资讯
			case 6:
				if(hasNews == true) {
					Titanium.App.Properties.setString('news_display_type', 'fund_detail');
					//2012.6.12. ac #126 基金内页，相关新闻列表进入后头部显示“XX基金相关新闻”
					title = FundCompany + '基金相关资讯';
					//2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
					openWindow('../news/tabNewsList.js', true);
					break;
				}
		}
	}
	//根据是否是货币基金添加不同的监听函数
	if(isCurrency) {
		tableviewFund.addEventListener('click', currencyListner);
	} else {
		tableviewFund.addEventListener('click', noCurrencyListner);
	}
	win.add(tableviewFund);
}

/* 打开新窗口
 * 传入：JS文件名
 * 传出：无
 */
function openWindow(fileName, simpleTitle) {
	var win = wing.ui.createWindow({
		url : fileName,
		barColor : 'faa61a'
	});
	//2012.6.14. ac #128 基金内页，基金名称与基金代码在头部固定
	if(simpleTitle != null && simpleTitle == true)
		win.title = title;
	win.backgroundImage = GetBackgroundFile();
	Titanium.UI.currentTab.open(win, {
		animated : true
	});
}

//响应修改背景
Titanium.App.addEventListener('app:background_changed', function(e) {
	win.backgroundImage = GetBackgroundFile(2);
});
